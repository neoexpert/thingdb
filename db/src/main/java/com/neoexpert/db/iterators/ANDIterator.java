/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db.iterators;

import com.neoexpert.db.index.btree.BTreeIndex;
import java.io.IOException;
import java.util.*;

/**
 * Hello world!
 *
 */
public class ANDIterator implements Iterator<BTreeIndex.Entry> {

	//DB db;
	private BTreeIndex.Entry next;

	

	ArrayList<Helper> its = new ArrayList<>();

	/**
	 *
	 * @param its
	 * @throws IOException
	 */
	public ANDIterator(ArrayList<Helper> its) throws IOException {
		//this.db = db;
		this.its=its;
		getNext();
	}

	/**
	 *
	 * @return
	 */
	@Override
	public boolean hasNext() {
		return next != null;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public BTreeIndex.Entry next() {
		try {
			BTreeIndex.Entry e = next;
			getNext();
			return e;
		} catch (IOException ex) {
			throw new RuntimeException("IOException");
		}
	}

	@Override
	public void remove() {

	}

	private void getNext() throws IOException {
		loop:
		for (int i=0;i<its.size();i++) {
			Helper it=its.get(i);
			if (!it.hasNext()) {
				if (it.isEmpty()) {
					next = null;
					return;
				}
				continue;
			}
			BTreeIndex.Entry e = it.next();
			for (Helper jt : its) {
				if (!jt.contains(e.value)) {
					continue loop;
				}
			}

			next = e;
			
			for(Helper jt:its)
				jt.remove(e.value);
		}
	}
}
