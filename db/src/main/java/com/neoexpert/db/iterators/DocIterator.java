/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db.iterators;

import com.neoexpert.db.DB;
import com.neoexpert.db.Doc;
import com.neoexpert.db.Q;
import com.neoexpert.db.index.Index;
import com.neoexpert.db.index.btree.BTreeIndex;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

/**
 * Hello world!
 *
 */
public class DocIterator implements Iterator<Doc> {

	DB db;
	private Doc nextdoc;

	ArrayList<Helper> helper = new ArrayList<>();

	private final Doc q;

	/**
	 *
	 * @param db
	 * @param q
	 * @param sort
	 * @throws IOException
	 */
	public DocIterator(DB db, Doc q, Doc sort) throws IOException {
		this.db = db;
		this.q = q;
		Set<Integer> keys = q.hashKeySet();
		ArrayList<Integer> toremove = new ArrayList<>();
		ArrayList<Helper> its = new ArrayList<>();
		for (Integer k : keys) {
			Index i = db.getIndex(k);
			if (i == null) {
				continue;
			}
			Object v = q.getByHash(k);
			Iterator<BTreeIndex.Entry> iter;
			if (v instanceof Doc) {
				Integer key = ((Doc) v).getKey();
				switch (key) {
					case Q.$gt:
						boolean desc = false;
						if (sort.has(k)) {
							desc = ((int) sort.getByHash(k)) < 0;
						}
						iter = i.findGT(((Doc) v).getByHash(key), desc);
						break;
					case Q.$lt:
						desc = false;
						if (sort.has(k)) {
							desc = ((int) sort.getByHash(k)) < 0;
						}
						iter = i.findLT(((Doc) v).getByHash(key), desc);
						break;
					case Q.$exists:
						desc = false;
						if (sort.has(k)) {
							desc = ((int) sort.getByHash(k)) < 0;
						}
						iter = i.find();
						break;

					default:
						throw new RuntimeException();
				}
			} else {
				iter = i.find(v);
			}
			toremove.add(k);
			its.add(new Helper(iter));
		}

		for (Integer k : toremove) {
			q.removeByHash(k);
		}

		if (its.isEmpty()) {
			helper.add(new Helper(db.getIndex(DB._id).findGT(0, false)));
		} else {
			helper.add(new Helper(new ANDIterator(its)));
		}
	}

	/**
	 *
	 * @return
	 */
	@Override
	public boolean hasNext() {
		if (nextdoc == null) {
			try {
				getNext();
			} catch (IOException ex) {
				throw new RuntimeException(ex);
			}
		}
		return nextdoc != null;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public Doc next() {
		try {
			if (nextdoc == null) {
				getNext();
			}
			Doc doc = nextdoc;
			nextdoc = null;
			return doc;
		} catch (IOException ex) {
			throw new RuntimeException("IOException");
		}
	}

	@Override
	public void remove() {

	}

	private void getNext() throws IOException {
		nextdoc = null;
		loop:
		for (int i = 0; i < helper.size(); i++) {
			Helper it = helper.get(i);
			if (!it.hasNext()) {
				if (it.isEmpty()) {
					nextdoc = null;
					return;
				}
				continue;
			}
			BTreeIndex.Entry e = it.next();
			for (Helper jt : helper) {
				if (!jt.contains(e.value)) {
					continue loop;
				}
			}

			nextdoc = db.findOne(e.value);
			if (!q.isEmpty()) {
				System.out.println("slow scan");
				if (!nextdoc.contains(q)) {
					nextdoc = null;
					i = -1;
					continue;
				}
			}
			for (Helper jt : helper) {
				jt.remove(e.value);
			}
		}
	}

	/**
	 *
	 * @return @throws IOException
	 */
	public int count() throws IOException {
		int c = 0;
		while (true) {
			loop:
			for (int i = 0; i < helper.size(); i++) {
				Helper it = helper.get(i);
				if (!it.hasNext()) {
					if (it.isEmpty()) {
						nextdoc = null;
						return c;
					}
					continue;
				}
				c++;

				BTreeIndex.Entry e = it.next();
				for (Helper jt : helper) {
					if (!jt.contains(e.value)) {
						continue loop;
					}
				}

				if (!q.isEmpty()) {
					nextdoc = db.findOne(e.value);
					if (!nextdoc.contains(q)) {
						nextdoc = null;
						i = -1;
						continue;
					}
				}
				for (Helper jt : helper) {
					jt.remove(e.value);
				}
			}
		}
	}
}
