/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db.iterators;

import com.neoexpert.db.index.btree.BTreeIndex;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author neoexpert
 */
public class Helper implements Iterator<BTreeIndex.Entry> {
	Set<Integer> _ids = new HashSet<>();
	Iterator<BTreeIndex.Entry> it;

	/**
	 *
	 * @param it
	 */
	public Helper(Iterator<BTreeIndex.Entry> it) {
		this.it = it;
	}

	/**
	 *
	 * @param _id
	 * @return
	 */
	public boolean contains(int _id) {
		return _ids.contains(_id);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public boolean hasNext() {
		return it.hasNext();
	}

	/**
	 *
	 * @return
	 */
	@Override
	public BTreeIndex.Entry next() {
		BTreeIndex.Entry e = it.next();
		_ids.add(e.value);
		return e;
	}

	@Override
	public void remove() {

	}

	boolean isEmpty() {
		return _ids.isEmpty();
	}

	void remove(int _id) {
		_ids.remove(_id);
	}
}
