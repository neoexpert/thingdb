/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db.index.btree;

import btree.BTree;
import btree.Traverser;
import com.neoexpert.db.index.Index;
import com.neoexpert.db.values.Value;
import java.io.*;
import java.util.*;

/**
 * BTree
 *
 *
 */
public class BTreeIndex extends BTree implements Index, AutoCloseable {


	/**
	 *
	 * @param file
	 * @param keysize
	 * @param unique
	 * @return
	 * @throws IOException
	 */
	public static BTreeIndex create(File file, int keysize, boolean unique) throws IOException {
		try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
			if (raf.length() != 0) {
				throw new RuntimeException("file is not empty!");
			}
			raf.writeInt(42);
			raf.writeInt(VERSION);
			raf.writeInt(keysize);
			raf.writeBoolean(unique);
			int node_bytes
					= 4
					+ 1
					+ (NODE_SIZE + 1) * keysize
					+ (NODE_SIZE + 1) * VALUE_SIZE
					+ NODE_POINTERS * 4;
			raf.setLength(node_bytes);
		}
		return new BTreeIndex(file);
	}

	/**
	 *
	 * @param file
	 * @throws IOException
	 */
	public BTreeIndex(File file) throws IOException {
	    super(file);
	}


	/**
	 *
	 * @param v
	 * @param value
	 * @throws IOException
	 */
	@Override
	public void add(Object v, int value) throws IOException {
		addA(Value.get(v).getBytes(), value);
	}



	/**
	 *
	 * @param k
	 * @return
	 * @throws IOException
	 */
	@Override
	public Integer get(Object k) throws IOException {
		return get(Value.get(k).getBytes());
	}
	
	
	/**
	 *
	 * @param k
	 * @param val
	 * @return
	 * @throws IOException
	 */
	public Integer get(Object k, int val) throws IOException {
		return get(Value.get(k).getBytes(),val);
	}
	

	/**
	 *
	 * @param k
	 * @return
	 * @throws IOException
	 */
	@Override
	public Integer delete(Object k) throws IOException {
		return delete(Value.get(k).getBytes());
	}

	/**
	 *
	 * @param k
	 * @param v
	 * @return
	 * @throws IOException
	 */
	@Override
	public synchronized Integer delete(Object k, int v) throws IOException {
		return delete(Value.get(k).getBytes(), v);
	}

	/**
	 *
	 * @param k
	 * @return
	 * @throws IOException
	 */
	public boolean contains(Object k) throws IOException {
		return containsA(Value.get(k).getBytes());
	}


	/**
	 *
	 * @param t
	 * @param key
	 * @throws IOException
	 */
	public void traverseGT(Traverser t, Object key) throws IOException {
		traverseGT(t, Value.get(key).getBytes());
	}

	/**
	 *
	 * @param t
	 * @param k
	 * @throws IOException
	 */
	public void findLT(Traverser t, int k) throws IOException {
		findLT(t, Value.get(k).getBytes());
	}

	/**
	 *
	 * @param k
	 * @param desc
	 * @return
	 * @throws IOException
	 */
	@Override
	public Iterator<Entry> findGT(Object k, boolean desc) throws IOException {
		return findGT(Value.get(k).getBytes(), desc);
	}

	/**
	 *
	 * @param k
	 * @param desc
	 * @return
	 * @throws IOException
	 */
	@Override
	public Iterator<Entry> findLT(Object k, boolean desc) throws IOException {
		return findLT(Value.get(k).getBytes(), desc);
	}

	/**
	 *
	 * @param k
	 * @return
	 * @throws IOException
	 */
	@Override
	public Iterator<Entry> find(Object k) throws IOException {
		return find(Value.get(k).getBytes());
	}


	/**
	 *
	 * @param key
	 * @return
	 * @throws IOException
	 */
	public int count(int key) throws IOException {
		int c = 0;
		Iterator<Entry> it = find(key);
		if (it == null) {
			return 0;
		}
		while (it.hasNext()) {
			c++;
			it.next();
		}
		return c;
	}

	


	private final static char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();

	/**
	 *
	 * @param bytes
	 * @return
	 */
	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = HEX_CHARS[v >>> 4];
			hexChars[j * 2 + 1] = HEX_CHARS[v & 0x0F];
		}
		return new String(hexChars);
	}
}
