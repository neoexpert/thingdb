/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db.values;

import java.nio.ByteOrder;

/**
 *
 * @author neoexpert
 */
public class VBoolean extends Value {

	private static final ByteOrder BYTE_ORDER = ByteOrder.BIG_ENDIAN;
	private byte v;

	/**
	 *
	 * @param v
	 */
	public VBoolean(Boolean v) {
		this.v=(byte) (v?1:0);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public byte[] getBytes() {
		return new byte[]{v};
	}

	/**
	 *
	 * @param arr
	 */
	@Override
	public final void fromByteArray(byte[] arr) {
		v = java.nio.ByteBuffer.wrap(arr).order(BYTE_ORDER).get();
	}
}
