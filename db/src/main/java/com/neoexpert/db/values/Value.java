/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db.values;

import com.neoexpert.db.Doc;

/**
 *
 * @author neoexpert
 */
public abstract class Value {

	/**
	 *
	 * @return
	 */
	public abstract byte[] getBytes();

	/**
	 *
	 * @param arr
	 */
	public abstract void fromByteArray(byte[] arr);

	/**
	 *
	 * @param o
	 * @return
	 */
	public static Value get(Object o) {
		if (o instanceof String) {
			return new VString((String) o);
		}
		if (o instanceof Integer) {
			return new VInteger((Integer) o);
		}
		if (o instanceof Long) {
			return new VLong((Long) o);
		}
		if (o instanceof Double) {
			return new VDouble((Double) o);
		}
		if (o instanceof Float) {
			return new VFloat((Float) o);
		}
		if (o instanceof Boolean) {
			return new VBoolean((Boolean) o);
		}
		if (o instanceof byte[]) {
			return new VArray((byte[]) o);
		}
		
		throw new RuntimeException("cannot parse "+o);
	}
}
