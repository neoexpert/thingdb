/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db.values;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author neoexpert
 */
public class VInteger extends Value {

	private static final ByteOrder BYTE_ORDER = ByteOrder.BIG_ENDIAN;

	Integer v;

	/**
	 *
	 * @param v
	 */
	public VInteger(Integer v) {
		this.v = v;
	}

	/**
	 *
	 * @param arr
	 */
	public VInteger(byte[] arr) {
		fromByteArray(arr);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public byte[] getBytes() {
		return ByteBuffer.allocate(4).order(BYTE_ORDER).putInt(v).array();
	}

	/**
	 *
	 * @param arr
	 */
	@Override
	public final void fromByteArray(byte[] arr) {
		v = java.nio.ByteBuffer.wrap(arr).order(BYTE_ORDER).getInt();
	}

}
