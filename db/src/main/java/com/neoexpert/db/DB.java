/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db;

import com.neoexpert.db.index.*;
import com.neoexpert.db.index.btree.BTreeIndex;
import com.neoexpert.db.iterators.DocIterator;
import com.neoexpert.db.storage.*;
import java.io.*;
import java.util.*;
import org.json.JSONObject;

/**
 * Hello world!
 *
 */
public class DB implements AutoCloseable {
	private Storage s;
	private JSONObject indexes;
	private JSONObject meta;
	private HashMap<Integer, Index> ix = new HashMap<>();
	private StringHashSet keys;
	private final String name;

	/**
	 *
	 */
	public static final int _id = "_id".hashCode();

	/**
	 *
	 * @throws IOException
	 */
	public DB() throws IOException {
		this("./db");
	}

	/**
	 *
	 * @param path
	 * @throws IOException
	 */
	public DB(String path) throws IOException {
		this.name = path;
		File d = new File(path);
		ensureDirectory(d);
		s = new Storage(d, this);

		File metafile = new File(d, "meta");
		if (metafile.exists()) {
			try (BufferedReader br = new BufferedReader(new FileReader(metafile))) {
				meta = new JSONObject(br.readLine());
				indexes = meta.getJSONObject("indexes");
				Set<String> keys = indexes.keySet();
				for (String i : keys) {
					File ib = new File(d, "indexes/" + i);
					if (!ib.exists()) {
						createIndex(i, indexes.getBoolean(i));
					}

					BTreeIndex b = new BTreeIndex(ib);
					ix.put(i.hashCode(), b);
				}
			}
		} else {
			meta = new JSONObject();
			indexes = new JSONObject();
			meta.put("indexes", indexes);
			writeMeta();
			createIndex("_id", true);
		}
		File kd = new File(d, "keys");
		ensureDirectory(kd);
		keys = new StringHashSet(kd, "keys");

	}

	private void ensureDirectory(File d) throws IOException {
		if (d.exists() && !d.isDirectory()) {
			throw new RuntimeException(d + " is not a directory");
		}
		if (!d.exists()) {
			if (!d.mkdir()) {
				throw new IOException("could not mkdir " + d);
			}
		}
	}

	/**
	 *
	 * @param field
	 * @throws IOException
	 */
	public final void createIndex(String field) throws IOException {
		createIndex(field, false);
	}

	/**
	 *
	 * @param field
	 * @param unique
	 * @throws IOException
	 */
	public final void createIndex(String field, boolean unique) throws IOException {
		createIndex(field,unique,16);
	}
	/**
	 *
	 * @param field
	 * @param unique
	 * @throws IOException
	 */
	public final void createIndex(String field, boolean unique, int length) throws IOException {
		indexes.put(field, unique);
		writeMeta();
		File d = new File(name + "/indexes");
		ensureDirectory(d);
		File f = new File(d, field);
		if (f.exists()) {
			if (!f.delete()) {
				throw new IOException("could not delete old index file: " + f);
			}
		}
		BTreeIndex b = BTreeIndex.create(f, 128, unique);
		int i = 1;
		while (true) {
			Doc doc = findOne(i);
			if (doc == null) {
				break;
			}
			if (doc.has(field)) {
				Object v = doc.get(field);
				b.add(v, i);
			}
			i++;
		}
		ix.put(field.hashCode(), b);
	}

	private void writeMeta() throws IOException {
		File metafile = new File(name + "/meta");
		try (PrintWriter f0 = new PrintWriter(new FileWriter(metafile))) {
			f0.println(meta.toString());
		}
	}

	/**
	 *
	 * @param doc
	 * @return
	 * @throws IOException
	 */
	public Doc insert(Doc doc) throws IOException {
		doc.setDB(this);
		int id = s.save(doc);
		Set<Integer> ks = doc.hashKeySet();
		for (Integer key : ks) {
			Index i = ix.get(key);
			if (i != null) {
				i.add(doc.getByHash(key), id);
			}
		}

		return doc;
	}

	public Doc upsert(Doc q,Doc u) throws IOException {
		Doc d=findOne(q);
		if(d==null){
			q=processUpdate(q,u,false);
			System.out.println(q);
			return insert(q);
		}
		else{
			return updateLoadedOne(d,u);
		}
	}

	/**
	 *
	 * @param q
	 * @param u
	 * @return
	 * @throws IOException
	 */
	public Doc updateOne(Doc q, Doc u) throws IOException {
		Doc olddoc = findOne(q);
		if (olddoc == null) {
			return null;
		}

		return updateLoadedOne(olddoc, u);
	}

	private Doc processUpdate(Doc olddoc,Doc u,boolean updateindex)throws IOException{
		Doc set = (Doc) u.get("$set");
		Doc unset = (Doc) u.get("$unset");
		Doc inc = (Doc) u.get("$inc");
		Doc newdoc = new Doc(this);
		newdoc.merge(olddoc);
		if (set != null) {
			Collection<String> ks = set.keySet();
                       for (String k : ks) {
                               newdoc.put(k, set.get(k));
			       if(updateindex){
				       Index i = ix.get(k.hashCode());
				       if (i != null) {
					       i.delete(set.get(k), olddoc.getID());
					       i.add(set.get(k), olddoc.getID());
				       }
			       }
			}
		}
		if (unset != null) {
			Collection<String> ks = unset.keySet();
			for (String k : ks) {
				newdoc.remove(k);
				if(updateindex){
					Index i = ix.get(k.hashCode());
					if (i != null) {
						i.delete(unset.get(k), olddoc.getID());
					}
				}
			}
		}

		if (inc != null) {
			Collection<String> ks = inc.keySet();
			for (String k : ks) {
                               	Index i = ix.get(k.hashCode());
				Object po=newdoc.get(k);
				if(po==null)
					po=0;
				else if(i!=null&&updateindex)
                                       i.delete(po, olddoc.getID());
				if (po instanceof Integer) {
					System.out.println("po before: "+po);
					po=((Integer)po)+((Integer)inc.get(k));
					System.out.println("po after: "+po);
					newdoc.put(k,po);
				}
                               	if (i != null&&updateindex) {
                                       i.add(po, olddoc.getID());
                               	}
			}
		}
		return newdoc;
	}
	private Doc updateLoadedOne(Doc olddoc, Doc u) throws IOException {
		Doc newdoc=processUpdate(olddoc,u,true);

		s.update(olddoc, newdoc);
		return newdoc;
	}

	/**
	 *
	 * @param q
	 * @param u
	 * @return
	 * @throws IOException
	 */
	public int update(Doc q, Doc u) throws IOException {

		int count = 0;

		Iterator<Doc> it = find(q);

		while (it.hasNext()) {
			Doc olddoc = it.next();
			updateLoadedOne(olddoc, u);
			count++;
		}
		return count;
	}

	/**
	 *
	 * @param q
	 * @return
	 * @throws IOException
	 */
	public Doc findOne(Doc q) throws IOException {
		Set<Integer> ks = q.hashKeySet();
		if (ks.contains(_id)) {
			return findOne((Integer) q.getByHash(_id));
		}
		//check for indexes
		for (Integer key : ks) {
			Index i = ix.get(key);
			if (i == null) {
				continue;
			}
			Integer pos;
			Object v = q.getByHash(key);
			if (v instanceof Doc) {
				pos = null;

			} else {
				pos = i.get(v);
			}

			if (pos != null) {
				return findOne(pos);
			}
			return null;
		}
		switch (ks.size()) {
			case 1:
				Integer key = ks.iterator().next();
				return findOne(key, q.getByHash(key));
		}
		return scan(ks, q);
	}

	int scani = 1;

	/**
	 *
	 * @param keys
	 * @param q
	 * @return
	 * @throws IOException
	 */
	private Doc scan(Set<Integer> keys, Doc q) throws IOException {
		int scanj = scani;
		while (true) {
			Doc jo1 = findOne(scani++);
			Doc jo2 = findOne(scanj--);
			if (jo1 == null && jo2 == null) {
				break;
			}
			if (check(keys, jo1, q)) {
				return jo1;
			}
			if (check(keys, jo2, q)) {
				return jo2;
			}
		}
		return null;
	}

	private boolean check(Set<Integer> keys, Doc jo, Doc q) {
		//Set<Integer> jokeys = jo.hashKeySet();
		for (Integer key : keys) {
			Object o = jo.getByHash(key);
			if (o == null) {
				return false;
			}
			if (!o.equals(q.getByHash(key))) {
				return false;
			}
		}
		return true;
	}

	/**
	 *
	 * @param key
	 * @param value
	 * @return
	 * @throws IOException
	 */
	public Doc findOne(String key, Object value) throws IOException {
		return findOne(key.hashCode(), value);
	}

	/**
	 *
	 * @param key
	 * @param value
	 * @return
	 * @throws IOException
	 */
	private Doc findOne(int key, Object value) throws IOException {
		int prevscani = scani;
		int scanj = scani - 1;
		//System.out.println(scanj+"\t:"+scani);
		while (true) {

			//System.out.println(scanj+"\t:"+scani);
			Boolean jo1 = checkOne(scani, key, value);

			if (jo1 != null) {
				if (jo1) {
					Doc d = findOne(scani);
					scani += (prevscani - scani) / 2;
					return d;
				}
				scani++;
			}
			Boolean jo2 = checkOne(scanj, key, value);
			if (jo2 != null) {
				if (jo2) {
					Doc d = findOne(scanj);
					scani = scanj;
					scani += (prevscani - scanj) / 2;

					return d;
				}
				scanj--;
			}
			if (jo1 == null && jo2 == null) {
				break;
			}
		}
		return null;
	}

	/**
	 * find a doc in O(1)
	 *
	 * @param _id
	 * @return
	 * @throws IOException
	 */
	public Doc findOne(int _id) throws IOException {
		return s.load(_id, this);
	}

	/**
	 *
	 * @param q
	 * @return
	 * @throws IOException
	 */
	public Doc delete(Doc q) throws IOException {
		Doc r = new Doc();
		Iterator<Doc> it = find(q);
		while (it.hasNext()) {
			deleteLoadedOne(it.next());
		}
		return r;
	}

	private void deleteLoadedOne(Doc doc) throws IOException {
		if (doc == null) {
			return;
		}
		s.delete(doc);
		int id = doc.getID();
		Set<Integer> keyss = doc.hashKeySet();

		for (Integer key : keyss) {
			Index i = ix.get(key);
			if (i == null) {
				continue;
			}
			i.delete(doc.getByHash(key), id);
		}
	}

	/**
	 *
	 * @param id
	 * @param key
	 * @param v
	 * @return
	 * @throws IOException
	 */
	private Boolean checkOne(int id, int key, Object v) throws IOException {
		return s.checkOne(id, key, v);
	}

	/**
	 *
	 * @param q
	 * @return
	 * @throws IOException
	 */
	public Iterator<Doc> find(Doc q) throws IOException {
		return new DocIterator(this, q, null);
	}
	
	/**
	 *
	 * @param q
	 * @param sort
	 * @return
	 * @throws IOException
	 */
	public Iterator<Doc> find(Doc q, Doc sort) throws IOException {
		return new DocIterator(this, q, sort);
	}

	/**
	 *
	 * @param q
	 * @return
	 * @throws IOException
	 */
	public int count(Doc q) throws IOException {
		return new DocIterator(this, q,null).count();
	}

	/**
	 *
	 * @param keyname
	 * @throws IOException
	 */
	public void dropIndex(String keyname) throws IOException {
		indexes.remove(keyname);
		writeMeta();
		ix.remove(keyname.hashCode());
		File f = new File("./db/" + name + "/indexes/" + keyname);
		if (f.exists()) {
			if (!f.delete()) {
				throw new IOException("could not delete index file: " + f);
			}
		}
		File d = f.getParentFile();
		String[] list = d.list();
		if (list == null) {
			return;
		}
		if (d.list().length == 0) {
			if (!d.delete()) {
				throw new IOException("could not delete index directory " + d);
			}

		}
	}

	/**
	 *
	 * @param hashcode
	 * @return
	 */
	protected String getKey(int hashcode) {
		try {
			return keys.get(hashcode);
		} catch (IOException e) {
			e.printStackTrace(System.err);
			throw new RuntimeException("IOException");
		}
	}

	/**
	 *
	 * @param key
	 * @throws IOException
	 */
	protected void addKey(String key) throws IOException {
		if (key == null) {
			return;
		}
		keys.add(key);
	}
	
	

	

	/**
	 *
	 * @param q
	 * @throws IOException
	 */
	public void deleteOne(Doc q) throws IOException {
		deleteLoadedOne(findOne(q));
	}

	void dumpHoles() throws IOException {
		s.dumpHoles();
	}

	/**
	 *
	 * @throws Exception
	 */
	@Override
	public void close() throws Exception {
		s.close();
		keys.close();
		for (Index i : ix.values()) {
			i.close();
		}
	}

	/**
	 *
	 * @param k
	 * @return
	 */
	public Index getIndex(Integer k) {
		return ix.get(k);
	}

	public Doc check() throws IOException {
		return s.check();
	}
	public void defrag() throws IOException{
		s.defrag();
	}
}
