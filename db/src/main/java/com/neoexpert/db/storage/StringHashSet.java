/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db.storage;

import java.io.*;

/**
 * Hello world!
 *
 */
public class StringHashSet implements AutoCloseable{

	RandomAccessFile raf;
	DHashMap index;

	/**
	 *
	 * @param d
	 * @param name
	 * @throws IOException
	 */
	public StringHashSet(File d,String name) throws IOException {
		raf = new RandomAccessFile(new File(d, name), "rw");
		index = new DHashMap(new File(d,name+ "_index"));
	}

	/**
	 *
	 * @param value
	 * @throws IOException
	 */
	public void add(String value) throws IOException {
		int pos = (int)raf.length();
		index.put(value.hashCode(), pos);

		raf.seek(raf.length());

		byte[] bytes=value.getBytes();
		raf.writeInt(bytes.length);
		raf.write(bytes);
	}

	/**
	 *
	 * @param value
	 * @throws IOException
	 */
	public void delete(String value) throws IOException {
		delete(value.hashCode());
	}

	/**
	 *
	 * @param key
	 * @throws IOException
	 */
	public void delete(int key) throws IOException {
		index.delete(key);
	}

	/**
	 *
	 * @param _id
	 * @return
	 * @throws IOException
	 */
	public String get(int _id) throws IOException {
		if (_id == 0) {
			return null;
		}
		Integer pos = index.get(_id);
		if (pos == null) {
			return null;
		}
		raf.seek(pos);
		byte[] bytes=new byte[raf.readInt()];
		raf.read(bytes);
		return new String(bytes);
	}

	/**
	 *
	 * @throws Exception
	 */
	@Override
	public void close() throws Exception {
		raf.close();
		index.close();
	}
}
