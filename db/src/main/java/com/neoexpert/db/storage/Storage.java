/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db.storage;

import com.neoexpert.db.*;
import com.neoexpert.db.index.btree.BTreeIndex;
import java.io.*;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * struct Hole { // (4 bytes)to identify a hole the length will be saved as
 * negative value: [-2147483648,-4] int length; // (-length-8 bytes) exists only
 * if the length of the hole is greater than 8 bytes byte
 * empty_space[-length-8]; // (4 bytes) exists only if the length of the hole is
 * greater than 4 bytes int length;	 *
 * };
 */
public class Storage implements AutoCloseable {
	private static final Object LOCK = new Object();
	RandomAccessFile raf;
	DHashMap index;
	int lastID = 0;
	BTreeIndex holes;
	private static final int VERSION = 4;
	private static final int DOFFSET = 20;
	private final DB db;
	private int lastdefragpos;

	/**
	 *
	 * @param d
	 * @param db
	 * @throws IOException
	 */
	public Storage(File d, DB db) throws IOException {
		this.db = db;
		raf = new RandomAccessFile(new File(d, "db"), "rw");
		if (raf.length() == 0) {
			raf.writeInt(42);
			raf.writeInt(VERSION);
			raf.writeInt(lastID);
			raf.writeLong(DOFFSET);

		} else {
			if (raf.readInt() != 42) {
				throw new RuntimeException("unknown db file");
			}
			if (raf.readInt() != VERSION) {
				throw new RuntimeException("wrong db version");
			}
			if (raf.length() < DOFFSET) {
				throw new RuntimeException("db file too short");
			}

			lastID = raf.readInt();
			long last_length = raf.readLong();
			if (last_length != raf.length()) {
				raf.setLength(last_length);
				System.out.println("db file may be corrupt, please repair");
			}
		}
		index = new DHashMap(new File(d, "index"));

		holes = new BTreeIndex(new File(d, "holes"));
	}

	/**
	 *
	 * @param doc
	 * @return
	 * @throws IOException
	 */
	public int save(Doc doc) throws IOException {
		synchronized (LOCK) {
			lastID++;
			raf.seek(8);
			raf.writeInt(lastID);
		}
		int _id = lastID;
		synchronized (LOCK) {
			save(_id, doc);
			db.getIndex(DB._id).add(_id, _id);
			return _id;
		}
	}

	private void save(int _id, Doc doc) throws IOException {
		synchronized (LOCK) {
			int pos = (int) raf.length();
			if (pos % 4 != 0) {
				pos = (int) (raf.length() / 4 + 1) * 4;
				//something went wrong:
				checkAsync();
			}

			raf.seek(pos);

			//CRITICAL CODE BLOCK:
			{
				try {
					int length = doc.write(_id, raf);
					index.delete(_id);
					index.put(_id, pos);
				} catch (Exception e) {
					raf.setLength(pos);
					throw new RuntimeException("could not write doc", e);
				}
				writeCurrentLength();
			}

			defragsleep = true;
		}
		defragASync();
		//defrag();

	}

	/**
	 *
	 * @param olddoc
	 * @param newdoc
	 * @throws IOException
	 */
	public void update(Doc olddoc, Doc newdoc) throws IOException {
		synchronized (LOCK) {
			Integer pos = index.get(olddoc.getID());
			save(olddoc.getID(), newdoc);

			delete(pos);
		}
	}

	/**
	 *
	 * @param _id
	 * @param db
	 * @return
	 * @throws IOException
	 */
	public Doc load(int _id, DB db) throws IOException {
		synchronized (LOCK) {
			if (_id == 0) {
				return null;
			}
			Integer pos = index.get(_id);
			//System.out.println("trying to load " + _id + " from " + pos);

			if (pos == null) {
				return null;
			}
			raf.seek(pos);

			Doc doc = new Doc(db).read(raf);

			if (_id != doc.getID()) {
				throw new RuntimeException("loaded _id is " + doc.getID() + ", but accquired _id was " + _id);
			}
			defragsleep = true;

			return doc;
		}
	}

	public void delete(Doc doc) throws IOException {
		synchronized (LOCK) {
			Integer pos = index.delete(doc.getID());
			delete(pos);
			db.getIndex(DB._id).delete(doc.getID());
		}
	}

	/**
	 *
	 * @param pos
	 * @throws IOException
	 */
	private void delete(int pos) throws IOException {
		synchronized (LOCK) {
			raf.seek(pos);
			int holeseize = raf.readInt();
			if (holeseize < 0) {
				getClass();
			}

			////Hole merging begin /////////
			//check for hole before the doc
			if (pos > DOFFSET) {

				raf.seek(pos - 4);
				int prev_rlength = raf.readInt();

				if (prev_rlength < 0) {
					int prev_pos = pos + prev_rlength;

					raf.seek(prev_pos);

					int prev_llength = raf.readInt();

					if (prev_llength != prev_rlength) {
						throw new RuntimeException("wrong hole");
					}
					//System.out.println("hole merged with");
					prev_rlength = -prev_rlength;
					pos -= prev_rlength;

					if (!holes.delete(prev_rlength, pos).equals(pos)) {
						throw new RuntimeException("corrupt db file");
					}

					holeseize += prev_rlength;
					raf.seek(pos + holeseize - 4);
					raf.writeInt(holeseize);
				}
			}

			//check for hole after the doc
			int next_pos = pos + holeseize;
			if (raf.length() > next_pos) {

				raf.seek(next_pos);
				int next_llength = raf.readInt();
				if (next_llength < 0) {

					if (!holes.delete(-next_llength, next_pos).equals(pos + holeseize)) {
						throw new RuntimeException("corrupt db file");
					}

					//System.out.println("hole merged");
					holeseize += -next_llength;
					raf.seek(pos + holeseize - 4);
					raf.writeInt(holeseize);
				}
			}

			////Hole merging end/////////
			addHole(holeseize, pos);

		}
		defragASync();

	}

	/**
	 *
	 * @param _id
	 * @param key
	 * @param v
	 * @return
	 * @throws IOException
	 */
	public Boolean checkOne(int _id, int key, Object v) throws IOException {
		if (_id == 0) {
			return null;
		}
		synchronized (LOCK) {

			Integer pos = index.get(_id);
			if (pos == null) {
				return null;
			}
			raf.seek(pos);
			return v.equals(Doc.getValue(raf, key));
		}
	}

	/**
	 *
	 * @throws IOException
	 */
	public void dumpHoles() throws IOException {
		synchronized (LOCK) {
			Iterator<BTreeIndex.Entry> it = holes.findGT(0, false);
			if (it != null) {
				while (it.hasNext()) {
					BTreeIndex.Entry e = it.next();
					System.out.println(e.getKeyAsInt() + " - " + e.value);
				}
			}
		}
	}
	Thread t;
	private boolean defrag = false;
	private static boolean threadrunning = false;

	private void defragASync() {
		if (threadrunning) {
			return;
		}
		t = new Thread(new Runnable() {
			@Override
			public void run() {
				threadrunning = true;

				try {
					defrag();
				} catch (IOException ex) {
					Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
					threadrunning = false;
					throw new RuntimeException(ex);
				}
				threadrunning = false;
			}
		});
		t.setPriority(Thread.MIN_PRIORITY);
		t.start();
	}

	private int defragsleeptime = 1;
	private boolean defragsleep = true;

	private static class Data{
		private int _id;
		private byte[] data;
		private int length;
	}
	/**
	 *
	 * @throws IOException
	 */
	public void defrag() throws IOException {

		defrag = true;
		while (defrag) {
			if (defragsleep) {
				defragsleeptime += 16;
				if (defragsleeptime > 500) {
					defragsleeptime = 500;
				}
				defragsleep = false;
			} else {
				if (defragsleeptime > 0) {
					System.out.println("sleep: " + defragsleeptime);
				}
				defragsleeptime /= 2;
			}
			if (defragsleeptime > 0) {
				try {
					Thread.sleep(defragsleeptime);
				} catch (InterruptedException e) {
				}
			}
			synchronized (LOCK) {
				if (lastdefragpos > raf.length()) {
					lastdefragpos = DOFFSET;
				}
				raf.seek(lastdefragpos);
				int llength = raf.readInt();
				if (llength < 0) {
					holes.get(3);
				} else {
					lastdefragpos += llength;
				}
				gapeSmallestHole(0);
			}

			synchronized (LOCK) {

				Data ld = loadlastdoc();
				if (ld == null) {
					continue;
				}
				//int _id = ld.getID();

				int length = ld.length;

				Iterator<BTreeIndex.Entry> it = holes.findGT(length, false);
				if (it != null) {
					if (it.hasNext()) {
						BTreeIndex.Entry e = it.next();
						int pos = e.value;
						int holesize = e.getKeyAsInt();
						moveDocToHole(ld, pos, holesize);

						raf.setLength(raf.length() - length);

						writeCurrentLength();

					} else {
						if (!defragsleep) {
							if (gapeSmallestHole(length)) {
								continue;
							}
						}
						return;
					}
				} else {
					return;
				}

			}
		}

	}

	private Data loadlastdoc() throws IOException {
		Data ld=new Data();
		long pos1 = raf.length() - 4;

		raf.seek(pos1);
		int rlength = raf.readInt();
		if (rlength < 0) {
			//the smallest hole posible is 4 bytes long

			//CRITICAL CODE BLOCK:
			{
				raf.seek(raf.length() + rlength);
				int llength = raf.readInt();
				if (rlength != llength) {
					throw new RuntimeException("corrupt db file");
				}
				raf.setLength(raf.length() + rlength);
				holes.delete(-rlength, (int) raf.getFilePointer());
				writeCurrentLength();
			}
			return null;
		}

		long pos2 = raf.length() - rlength;
		if (pos2 < 0 || pos1 < 50) {
			defrag = false;
			return null;
		}
		
		raf.seek(pos2);
		ld.length = raf.readInt();
		if (rlength != ld.length) {
			throw new IOException("corupt length");
		}
		
		ld._id=raf.readInt();
		ld.data=new byte[rlength-12];
		raf.read(ld.data);
		return ld;
		/*
		int _id = raf.readInt();
		//System.out.println("pos1: " + pos1 + " ,pos2: " + pos2 + " ,length: " + length);
		//System.out.println("_id: " + _id);

		Doc doc = new Doc(db).read(_id, raf);
		long pos = raf.getFilePointer();
		if (pos % 4 != 0) {
			pos = ((pos / 4) + 1) * 4;
			raf.seek(pos);
		}

		

		return doc;
		*/
	}

	/**
	 *
	 * @throws IOException
	 */
	public Doc check() throws IOException {
		Doc result = new Doc();
		int holessize = 0;
		int biggest_hole = 0;
		int smallest_hole = Integer.MAX_VALUE;
		int holesamount = 0;
		double avgholesize = 0.0;
		synchronized (LOCK) {

			raf.seek(DOFFSET);

			//System.out.println("#CHECK########################################################");
			while (raf.getFilePointer() < raf.length()) {
				long pos = raf.getFilePointer();
				if (pos % 4 != 0) {
					pos = ((pos / 4) + 1) * 4;
					raf.seek(pos);
				}

				int length = raf.readInt();
				if (length < 0) {
					//System.out.println("hole " + -_id + " found at " + (raf.getFilePointer() - 4));
					raf.seek(pos - length - 4);
					if (raf.readInt() != length) {
						throw new RuntimeException("corrupt hole length");
					}
					holessize -= length;
					holesamount++;
					if (biggest_hole < -length) {
						biggest_hole = -length;
					}
					if (smallest_hole > -length) {
						smallest_hole = -length;
					}
					avgholesize += (-length - avgholesize);
					continue;
				}

				int _id = raf.readInt();

				//System.out.println("doc " + _id + " found at " + (raf.getFilePointer() - 4));
				Doc doc = new Doc(db).read(_id, raf);
				//System.out.println(doc.toString());
				pos = raf.getFilePointer();
				if (pos % 4 != 0) {
					pos = ((pos / 4) + 1) * 4;
					raf.seek(pos);
				}
				int rlength = raf.readInt();
				if (length != rlength) {
					throw new RuntimeException("corrupt length");
				}

				if (doc.getLength() != length) {
					throw new RuntimeException("doc.getLength()!=length (" + doc.getLength() + "!=" + length + ")");
				}
				if (_id < 0 && -_id != length) {
					throw new RuntimeException("_id < 0 && -_id != length (" + -_id + "!=" + length + ")");

				}

			}
			//System.out.println("#CHECK DONE##################################################");

		}
		result.put("holessize", holessize);
		result.put("holeamount", holesamount);
		result.put("biggest_hole", biggest_hole);
		result.put("smallest_hole", smallest_hole);
		result.put("avgholesize", avgholesize);

		return result;
	}

	/**
	 *
	 * @throws Exception
	 */
	@Override
	public void close() throws Exception {
		defrag = false;
		if (t != null) {
			t.join();
		}
		writeCurrentLength();
		synchronized (LOCK) {
			raf.close();
			index.close();
			holes.close();
		}
	}

	private void writeCurrentLength() throws IOException {
		synchronized (LOCK) {
			raf.seek(12);
			raf.writeLong(raf.length());
		}
	}

	//CRITICAL CODE BLOCK:
	private void moveDocToHole(int docpos, int holepos, int holesize) throws IOException {
		raf.seek(docpos);
		//int _id = raf.readInt();
		Data doc=new Data();

		doc.length = raf.readInt();
		doc._id=raf.readInt();
		doc.data=new byte[doc.length-12];
		raf.read(doc.data);
		
		Integer ipos = index.delete(doc._id);
		if (ipos != docpos) {
			throw new RuntimeException("corrupt db");
		}

		moveDocToHole(doc, holepos, holesize);
	}

	//CRITICAL CODE BLOCK:
	private void moveDocToHole(Data doc,int holepos, int holesize) throws IOException {
		raf.seek(holepos);
		int rhole_size = raf.readInt();
		if (-rhole_size != holesize) {
			throw new RuntimeException("wrong hole");
		}
		if (rhole_size > 0) {
			throw new RuntimeException("not a hole");
		}
		if(holesize<doc.length)
			throw new RuntimeException("doc is too big for that hole");

		raf.seek(holepos);
		raf.writeInt(doc.length);
		raf.writeInt(doc._id);
		raf.write(doc.data);
		raf.writeInt(doc.length);
		//raf.writeInt(doc.getID());
		//int length = (int) doc.write(doc.getID(), raf);

		if (holes.delete(holesize, holepos) != holepos) {
			throw new RuntimeException("corrupt db");
		}

		raf.seek(holepos+4);

		index.put(doc._id, holepos);

		if (holesize > doc.length) {
			int newholesize = holesize - doc.length;
			int pos = holepos + doc.length;

			addHole(newholesize, pos);

		}
	}

	private boolean gapeSmallestHole(int length) throws IOException {
		Iterator<BTreeIndex.Entry> it1 = holes.findGT(0, false);
		if (it1 == null) {
			return false;
		}
		while (it1.hasNext()) {
			BTreeIndex.Entry e = it1.next();
			int holesize = e.getKeyAsInt();
			int hole_pos = e.value;
			raf.seek(e.value - 4);
			int prev_length = raf.readInt();
			if (prev_length < 0) {
				System.out.println("SONDERFALL");
				if (holes.delete(holesize, hole_pos) != hole_pos) {
					throw new RuntimeException("corrupt db");
				}
				int newholesize = holesize - prev_length;
				int prev_pos = hole_pos + prev_length;
				if (holes.delete(-prev_length, prev_pos) != prev_pos) {
					throw new RuntimeException("corrupt db");
				}
				addHole(newholesize, prev_pos);
				return true;
			}

			Iterator<BTreeIndex.Entry> it2 = holes.findGT(prev_length, false);
			if (it2.hasNext()) {
				BTreeIndex.Entry e2 = it2.next();
				if (e2.equals(e)) {
					if (it2.hasNext()) {
						e2 = it2.next();
					} else {
						return false;
					}
				}
				int thisholesize = e2.getKeyAsInt();
				if (thisholesize - prev_length < holesize) {
					int prev_pos = hole_pos - prev_length;
					moveDocToHole(prev_pos, e2.value, thisholesize);
					if (holes.delete(holesize, hole_pos) != hole_pos) {
						throw new RuntimeException("corrupt db");
					}
					int newholesize = holesize + prev_length;
					addHole(newholesize, prev_pos);
					if (newholesize >= length) {
						System.out.println("newholesize: " + newholesize);
						return true;
					}
				}
			}
		}
		return false;
	}

	private void checkAsync() {
	}

	private void addHole(int holesize, int pos) throws IOException {
		holes.add(holesize, pos);
		raf.seek(pos);

		if (holesize > 4) {
			raf.writeInt(-holesize);
			raf.seek(pos + holesize - 4);
			raf.writeInt(-holesize);
		} else {
			raf.writeInt(-holesize);
		}

		checkHole(holesize, pos);
	}

	private void checkHole(int holesize, int pos) throws IOException {
		raf.seek(pos);
		int llength = raf.readInt();
		if (-llength != holesize) {
			throw new RuntimeException("corrupt hole");
		}
		raf.seek(pos - llength - 4);
		int rlength = raf.readInt();
		if (-rlength != holesize) {
			throw new RuntimeException("corrupt hole");
		}
	}
}
