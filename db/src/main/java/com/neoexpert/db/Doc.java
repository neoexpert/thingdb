/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db;

import java.io.*;
import java.util.*;
import org.json.JSONObject;

/**
 *	blocksize: 4 bytes
 *   	struct Doc {
 *			//(4 bytes) max length of the doc is 2147483647 bytes
 *   		int length;
 * 
 *			//(4 bytes)
 *   		int _id; 	
 * 
 *			//(4 bytes)
 *   		int keys_amount; 	
 * 
 *   		Key keys[keys_amount]
 * 
 *   		Value values[keys_amount]
 * 
 *			//(4 bytes)
 *   		int length; 		
 *		};
 *   	struct Key {
 *			//(4 bytes)
 *   		int key;		
 *			//(4 bytes)
 *   		int value_offset	
 *   	}
 *
 *   	struct String extends Value
 *   	{
 *			//(4 bytes)
 *   		int length		
 *			//(length bytes)
 *			byte bytes[length]	
 *   	}
 *   	struct ByteArray extends Value
 *   	{
 *			//(4 bytes)
 *   		int length		
 *			//(length bytes)
 *			byte bytes[length]	
 *   	}
 *   	struct Integer extends Value
 *   	{
 *			//(4 bytes)
 *			int value		
 *   	}
 *   	struct Long extends Value
 *   	{
 *			//(8 bytes)
 *			long value		
 *   	}
 *   	struct Float extends Value
 *   	{
 *			//(4 bytes)
 *			float value		
 *   	}
 *   	struct Double extends Value
 *   	{
 *			//(8 bytes)
 *			double value		
 *   	}
 *   	struct Boolean extends Value
 *   	{
 *			//(1 byte)
 *			byte value		
 *   	}
 *   	struct Doc extends Value
 *   	{
 *   		Doc doc;		
 *   	}
 *
 */
public class Doc {

	private static int _idhash = 94650;
	private int _id;

	int length;

	/**
	 *
	 * @return
	 */
	public int getID() {
		return _id;
	}

	/**
	 *
	 * @param _id
	 * @return
	 */
	public Doc setID(int _id) {
		this._id = _id;
		jo = null;
		return this;
	}

	HashMap<Integer, Object> hm = new HashMap<>();
	private final Map<Integer, String> keys = new HashMap<>();

	/**
	 * Creates Empty Doc
	 */
	public Doc() {
	}

	/**
	 *
	 * @param jo
	 */
	public Doc(String jo) {
		this(new JSONObject(jo));
	}

	/**
	 *
	 * @param jo
	 */
	public Doc(JSONObject jo) {
		Set<String> ks = jo.keySet();
		for (String k : ks) {
			Object value = jo.get(k);
			if (value instanceof JSONObject) {
				put(k, new Doc((JSONObject) value));
				continue;
			}
			put(k, value);
		}
	}

	/**
	 *
	 * @param db
	 */
	public Doc(DB db) {
		this.db = db;
	}

	/**
	 *
	 * @param key
	 * @param value
	 */
	public Doc(String key, Object value) {
		put(key, value);
	}

	/**
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	public final Doc put(String key, Object value) {
		int hash = key.hashCode();
		if (hash == _idhash) {
			return this;
		}

		keys.put(hash, key);
		hm.put(hash, value);
		jo = null;
		return this;
	}

	/**
	 *
	 * @param doc
	 * @return
	 */
	public final Doc merge(Doc doc) {
		keys.putAll(doc.keys);
		hm.putAll(doc.hm);
		jo = null;
		return this;
	}

	/**
	 *
	 * @param key
	 * @return
	 */
	public final Object get(String key) {
		return hm.get(key.hashCode());
	}

	/**
	 *
	 * @param hashcode
	 * @return
	 */
	public Object getByHash(int hashcode) {
		return hm.get(hashcode);
	}

	/**
	 *
	 * @param hashcode
	 * @param value
	 * @return
	 */
	public Doc putByHash(int hashcode, Object value) {
		if (hashcode == _idhash) {
			return this;
		}
		hm.put(hashcode, value);
		jo = null;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public Map<Integer, String> keys() {
		return keys;
	}

	/**
	 *
	 * @return
	 */
	public Integer getKey() {
		return keys.keySet().iterator().next();
	}

	/**
	 *
	 * @param keykey
	 * @return
	 */
	public String getKey(int keykey) {
		return keys.get(keykey);
	}

	/**
	 *
	 * @param key
	 * @return
	 */
	public boolean has(String key) {
		return hashKeySet().contains(key.hashCode());
	}

	/**
	 *
	 * @param key
	 * @return
	 */
	public boolean has(Integer key) {
		return hashKeySet().contains(key);
	}

	/**
	 *
	 * @param raf
	 * @param key
	 * @return
	 * @throws IOException
	 */
	public static Object getValue(RandomAccessFile raf, int key) throws IOException {
		//int _id =
		raf.readInt();
		raf.readInt();

		long begin = raf.getFilePointer();
		int size = raf.readInt();
		for (int i = 0; i < size; i++) {
			if (raf.readInt() == key) {
				raf.seek(begin + raf.readInt());
				return readValue(raf);
			}
			raf.readInt();
		}
		return null;
	}

	private static Object readValue(RandomAccessFile raf) throws IOException {
		int t = raf.read();
		switch (t) {
			case 1:
				byte[] s = new byte[raf.readInt()];
				int bytes = raf.read(s);
				if (bytes != s.length) {
					throw new IOException("corrupt Document");
				}
				return new String(s);
			case 2:
				s = new byte[raf.readInt()];
				bytes = raf.read(s);
				if (bytes != s.length) {
					throw new IOException("corrupt Document");
				}
				return s;
			case 3:
				return raf.readInt();
			case 4:
				return raf.readLong();
			case 5:
				return raf.readDouble();
			case 6:
				return raf.readFloat();
			case 7:
				return raf.readBoolean();

			case 42:
				return new Doc().read(0, raf);
		}
		throw new RuntimeException("unknown type-id: " + t);
	}

	public Doc read(RandomAccessFile raf) throws IOException {
		int first_length = raf.readInt();
		read(raf.readInt(), raf);
		if(first_length!=length)
			throw new IOException("wrong length of the doc");
		return this;
	}

	/**
	 *
	 * @param _id
	 * @param raf
	 * @return
	 * @throws IOException
	 */
	public Doc read(int _id, RandomAccessFile raf) throws IOException {
		this._id = _id;
		long begin = raf.getFilePointer();
		hm.clear();
		int size = raf.readInt();
		ArrayList<Integer> keys = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			keys.add(raf.readInt());
			raf.readInt();
		}
		for (int i = 0; i < size; i++) {
			hm.put(keys.get(i), readValue(raf));
		}
		int real_length = (int) (raf.getFilePointer() - begin);
		if (real_length % 4 != 0) {
			real_length = ((real_length / 4) + 1) * 4;
		}
		real_length += 12;
		length=real_length;
		
		return this;
	}

	public int write(int _id, RandomAccessFile out) throws IOException {
		this._id = _id;
		long begin = out.getFilePointer();
		out.writeInt(0);
		out.writeInt(_id);
		
		write(out);
		
		long pos = out.getFilePointer();
		if (pos % 4 != 0) {
			pos = (int) (pos/ 4 + 1) * 4;
			out.seek(pos);
		}
		
		length = (int) (out.getFilePointer() - begin);
		if (length % 4 != 0) {
			length = ((length / 4) + 1) * 4;
		}
		
		length += 4;
		out.writeInt(length);
		
		long tmp=out.getFilePointer();
		out.seek(begin);
		out.writeInt(length);
		out.seek(tmp);
		
		return length;
	}

	/**
	 *
	 * @param out
	 * @throws IOException
	 */
	private void write(RandomAccessFile out) throws IOException {
		Set<Integer> keys = hashKeySet();

		long begin = out.getFilePointer();

		out.writeInt(keys.size());
		for (Integer key : keys) {
			out.writeInt(key);
			out.writeInt(0);
		}
		long currentkey = 0;
		for (Integer key : keys) {
			db.addKey(getKey(key));
			Object o = hm.get(key);
			long pos = out.getFilePointer();
			out.seek(begin + currentkey * 8 + 8);
			currentkey++;

			out.writeInt((int) (pos - begin));
			out.seek(pos);
			if (o instanceof String) {
				out.write(1);
				byte[] bs = ((String) o).getBytes();
				out.writeInt(bs.length);

				out.write(bs);
				continue;
			}
			if (o instanceof byte[]) {
				out.write(2);
				byte[] bs = (byte[]) o;
				out.writeInt(bs.length);
				out.write(bs);
				continue;
			}
			if (o instanceof Integer) {
				out.write(3);
				out.writeInt((Integer) o);
				continue;
			}
			if (o instanceof Long) {
				out.write(4);
				out.writeLong((Long) o);
				continue;
			}
			if (o instanceof Double) {
				out.write(5);
				out.writeDouble((Double) o);
				continue;
			}
			if (o instanceof Float) {
				out.write(6);
				out.writeFloat((Float) o);
				continue;
			}
			if (o instanceof Boolean) {
				out.write(7);
				out.writeBoolean((Boolean) o);
				continue;
			}
			if (o instanceof Doc) {
				out.write(42);
				Doc doc = (Doc) o;
				doc.setDB(db)
						.write(out);
				continue;
			}
			throw new RuntimeException("could not save type: " + o.getClass().getName());
		}
	}
	private DB db;

	/**
	 *
	 * @param db
	 * @return
	 */
	protected Doc setDB(DB db) {
		this.db = db;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public Set<Integer> hashKeySet() {
		return hm.keySet();
	}

	/**
	 *
	 * @return
	 */
	public int getLength() {
		return (int) length;
	}

	/**
	 *
	 * @return
	 */
	public DB getDB() {
		return db;
	}

	/**
	 *
	 * @param key
	 */
	public void remove(String key) {
		hm.remove(key.hashCode());
		keys.remove(key.hashCode());
		jo = null;
	}

	/**
	 *
	 * @param k
	 */
	public void removeByHash(int k) {
		hm.remove(k);
		keys.remove(k);
	}

	/**
	 *
	 * @return
	 */
	public boolean isEmpty() {
		return hm.isEmpty();
	}

	/**
	 *
	 * @param doc
	 * @return
	 */
	public boolean contains(Doc doc) {
		Set<Integer> hkeys = doc.hashKeySet();
		for (Integer k : hkeys) {
			Object v = hm.get(k);
			if (!doc.getByHash(k).equals(v)) {
				return false;
			}
		}
		return true;
	}

	/**
	 *
	 * @return
	 */
	public Collection<String> keySet() {
		return keys.values();
	}

	/**
	 *
	 * @param db
	 * @return
	 */
	public JSONObject toJSONObject(DB db) {
		this.db = db;
		return toJSONObject();
	}
	JSONObject jo;

	/**
	 *
	 * @return
	 */
	public JSONObject toJSONObject() {
		if (jo != null) {
			return jo;
		}
		jo = new JSONObject();
		Set<Integer> dkeys = hashKeySet();
		for (Integer key : dkeys) {
			String skey = keys.get(key);
			Object v = hm.get(key);
			if (v instanceof Doc) {
				v = ((Doc) v).toJSONObject(db);
			}
			if (skey != null) {
				jo.put(skey, v);
			} else {
				jo.put(db.getKey(key), v);
			}
		}

		return jo;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public String toString() {
		return toJSONObject().toString();
	}

}
