/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db;

/**
 * Hello world!
 *
 */
public class Q{

	public static final int $gt = 37905;
	public static final int $lt = 38060;
	public static final int $ne = 38107;
	public static final int $exists = 596003200;
	public static final int $inc = 1176890;
	/**
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	public static Doc eq(String key, Object value){
		return new Doc(key,value);
	}

	/**
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	public static Doc gt(String key, Object value){
		return new Doc(key,new Doc("$gt",value));
	}

	/**
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	public static Doc lt(String key, Object value){
		return new Doc(key,new Doc("$lt",value));
	}

	/**
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	public static Doc ne(String key, Object value){
		return new Doc(key,new Doc("$ne",value));
	}
	
	/**
	 *
	 * @param docs
	 * @return
	 */
	public static Doc and(Doc ... docs){
		Doc q=new Doc();
		for(Doc d:docs){
			Integer key = d.getKey();
			q.putByHash(key,d.getByHash(key));
		}
		return q;
	}

	public static Doc set(Doc doc){
		return new Doc("$set",doc);
	}
	public static Doc inc(String field,int i){
		return new Doc("$inc",new Doc(field,i));
	}
	public static Doc inc(Doc doc){
		return new Doc("$inc",doc);
	}

	public static Doc exists(String field){
		return eq(field,eq("$exits",true));
	}
}
