/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db;

import static com.neoexpert.db.Q.*;
import java.io.*;
import java.util.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.junit.*;
import org.junit.runners.*;

/**
 * Unit test for simple App.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DBTest
		extends TestCase {

	private static final double nanoseconds_in_second = 1000000000.0;

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public DBTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(DBTest.class);
	}
	Random r = new Random();

	/**
	 * Extreme Test
	 */
	public void test2Intensive() throws Exception {
		System.out.println("inchash:"+"$inc".hashCode());
		long maxTime = 0L;
		long minTime = Long.MAX_VALUE;
		long avgTime = 0L;
		try (DB db = new DB("./target/nodes")) {
			int amount = 1000;
			String keyname = "keyname";

			db.dropIndex(keyname);

			System.out.println("inserting " + amount + " documents...");
			for (int i = 0; i < amount; i++) {
				Doc doc = genRandomDoc();
				doc.put(keyname, i);
				doc.put("key", "value");

				db.insert(doc);
			}

			System.out.println();
			System.out.println("find " + amount + " documents by " + keyname + " without index...");
			for (int i = 0; i < amount; i++) {
				Doc doc = new Doc();
				doc.put(keyname, i);
				//######################
				long t = System.nanoTime();

				doc = db.findOne(doc);
				t = System.nanoTime() - t;
				avgTime += (t - avgTime);

				if (maxTime < t) {
					maxTime = t;
				}
				if (minTime > t) {
					minTime = t;
				}
				//######################
				//System.out.println(doc);
				assertNotNull(doc);
				assertTrue(doc.has(keyname));
				assertEquals(doc.get(keyname), i);
				assertTrue(doc.has("key"));
				assertTrue(doc.get("key").equals("value"));
			}
			System.out.println("took: " + (avgTime * amount) / nanoseconds_in_second + " seconds");
			System.out.println("max_find_time: " + maxTime / nanoseconds_in_second + " seconds");
			System.out.println("min_find_time: " + minTime / nanoseconds_in_second + " seconds");
			System.out.println("avg_find_time: " + avgTime / nanoseconds_in_second + " seconds");

			maxTime = 0L;
			minTime = Long.MAX_VALUE;
			avgTime = 0;
			System.out.println();
			int ramount = (int) 100000 / amount + 1;
			System.out.println("find " + ramount + " documents by " + keyname + " without index randomly...");
			for (int i = 0; i < ramount; i++) {
				Doc doc = new Doc();
				int value = r.nextInt(amount);
				doc.put(keyname, value);
				//######################
				long t = System.nanoTime();

				Doc q = doc;
				doc = db.findOne(doc);
				if (doc == null) {
					doc = db.findOne(q);
				}

				assertNotNull("value: " + value, doc);
				t = System.nanoTime() - t;
				avgTime += (t - avgTime);

				if (maxTime < t) {
					maxTime = t;
				}
				if (minTime > t) {
					minTime = t;
				}
				//######################
				//System.out.println(doc);
				assertTrue(doc.has(keyname));
				assertTrue(doc.get(keyname).equals(value));
			}
			System.out.println("took: " + (avgTime * ramount) / nanoseconds_in_second + " seconds");

			System.out.println("max_find_time: " + maxTime / nanoseconds_in_second + " seconds");
			System.out.println("min_find_time: " + minTime / nanoseconds_in_second + " seconds");
			System.out.println("avg_find_time: " + avgTime / nanoseconds_in_second + " seconds");

			System.out.println();
			System.out.println("creating indexes...");
			db.createIndex(keyname);
			db.createIndex("key");

			maxTime = 0L;
			minTime = Long.MAX_VALUE;
			avgTime = 0;
			System.out.println();
			System.out.println("find " + amount + " documents by " + keyname + " with index randomly...");
			for (int i = 0; i < amount; i++) {
				Doc q = new Doc();
				int value = r.nextInt(amount);
				q.put(keyname, value);
				//######################
				long t = System.nanoTime();
				Doc doc = db.findOne(q);

				t = System.nanoTime() - t;
				avgTime += (t - avgTime);

				if (maxTime < t) {
					maxTime = t;
				}
				if (minTime > t) {
					minTime = t;
				}
				//######################
				//System.out.println(doc);
				assertTrue(doc.has(keyname));
				assertTrue(value + "!=" + doc.get(keyname) + ", " + i + "  " + doc, doc.get(keyname).equals(value));

				//assertTrue(value + "!=" + doc.get(keyname) + ", " + i + "  " + doc, doc.get(keyname).equals("9"));
			}

			System.out.println("took: " + (avgTime * amount) / nanoseconds_in_second + " seconds");

			System.out.println("max_find_time: " + maxTime / nanoseconds_in_second + " seconds");
			System.out.println("min_find_time: " + minTime / nanoseconds_in_second + " seconds");
			System.out.println("avg_find_time: " + avgTime / nanoseconds_in_second + " seconds");

			////////////////////////////////////
			System.out.println("update all docs WHERE key=value set added_key=value");

			int count = db.update(eq("key", "value"), new Doc("$set", new Doc("added_key", "value")));

			assertEquals(count, db.count(eq("key", "value")));

			System.out.println("check the updated docs...");

			Iterator<Doc> it = db.find(eq("key", "value"));
			while (it.hasNext()) {
				Doc doc = it.next();
				assertTrue(doc.has("added_key"));
				assertEquals(doc.get("added_key"), "value");
			}

			maxTime = 0L;
			minTime = Long.MAX_VALUE;
			avgTime = 0L;
			System.out.println();
			System.out.println("update " + amount + " documents by " + keyname + " with index randomly...");
			System.out.println();
			long lastt = System.currentTimeMillis();
			for (int i = 0; i < amount; i++) {
				if (System.currentTimeMillis() - lastt > 10000) {
					System.out.println((i * 100) / amount + " % (" + i + ")");
					System.out.flush();
					lastt = System.currentTimeMillis();
				}
				Doc q = new Doc();
				int value = r.nextInt(amount);
				q.put(keyname, value);
				//######################
				long t = System.nanoTime();
				db.updateOne(q, new Doc("$set", new Doc("addedkey", "value")));

				t = System.nanoTime() - t;
				avgTime += (t - avgTime);

				if (maxTime < t) {
					maxTime = t;
				}
				if (minTime > t) {
					minTime = t;
				}
				//######################
				//System.out.println(doc);

			}
			System.out.println();
			System.out.println("took: " + (avgTime * amount) / nanoseconds_in_second + " seconds");

			System.out.println("max_update_time: " + maxTime / nanoseconds_in_second + " seconds");
			System.out.println("min_update_time: " + minTime / nanoseconds_in_second + " seconds");
			System.out.println("avg_update_time: " + avgTime / nanoseconds_in_second + " seconds");
			///////////////////////////////////

			maxTime = 0L;
			minTime = Long.MAX_VALUE;
			avgTime = 0L;

			System.out.println();
			System.out.println("find " + amount + " documents by " + keyname + " with index...");
			for (int i = 0; i < amount; i++) {
				Doc doc = new Doc();
				doc.put(keyname, i);
				//######################
				long t = System.nanoTime();

				doc = db.findOne(doc);
				t = System.nanoTime() - t;
				avgTime += (t - avgTime);

				if (maxTime < t) {
					maxTime = t;
				}
				if (minTime > t) {
					minTime = t;
				}
				//######################
				//System.out.println(doc);
				assertTrue(doc.has(keyname));
				assertEquals(doc.get(keyname), i);
			}
			System.out.println("took: " + (avgTime * amount) / nanoseconds_in_second + " seconds");

			System.out.println("max_find_time: " + maxTime / nanoseconds_in_second + " seconds");
			System.out.println("min_find_time: " + minTime / nanoseconds_in_second + " seconds");
			System.out.println("avg_find_time: " + avgTime / nanoseconds_in_second + " seconds");

			maxTime = 0L;
			minTime = Long.MAX_VALUE;
			avgTime = 0;

			amount *= 2;
			System.out.println();
			System.out.println("inserting " + amount + " documents with index...");
			for (int i = 0; i < amount; i++) {
				Doc doc = genRandomDoc();
				doc.put(keyname, i);

				long t = System.nanoTime();
				db.insert(doc);
				t = System.nanoTime() - t;
				avgTime += (t - avgTime);

				if (maxTime < t) {
					maxTime = t;
				}
				if (minTime > t) {
					minTime = t;
				}

			}
			System.out.println("took: " + (avgTime * amount) / nanoseconds_in_second + " seconds");
			System.out.println("max_insert_time: " + maxTime / nanoseconds_in_second + " seconds");
			System.out.println("min_insert_time: " + minTime / nanoseconds_in_second + " seconds");
			System.out.println("avg_insert_time: " + avgTime / nanoseconds_in_second + " seconds");

			maxTime = 0L;
			minTime = Long.MAX_VALUE;
			avgTime = 0;
			System.out.println();
			System.out.println("find " + amount + " documents by " + keyname + " with index randomly...");
			for (int i = 0; i < amount; i++) {
				int value = r.nextInt(amount);
				Doc doc = new Doc();
				doc.put(keyname, value);
				//######################
				long t = System.nanoTime();

				doc = db.findOne(doc);
				t = System.nanoTime() - t;
				avgTime += (t - avgTime);

				if (maxTime < t) {
					maxTime = t;
				}
				if (minTime > t) {
					minTime = t;
				}
				//######################
				//System.out.println(doc);
				assertTrue("i: " + i + "doc: " + doc, doc.has(keyname));
				assertEquals(doc.get(keyname), value);
			}
			System.out.println("took: " + (avgTime * amount) / nanoseconds_in_second + " seconds");

			System.out.println("max_find_time: " + maxTime / nanoseconds_in_second + " seconds");
			System.out.println("min_find_time: " + minTime / nanoseconds_in_second + " seconds");
			System.out.println("avg_find_time: " + avgTime / nanoseconds_in_second + " seconds");
			System.out.println();
			db.check();

		}

	}

	public void test3UpdateDoc() throws IOException, Exception {

		long maxTime = 0L;
		long minTime = Long.MAX_VALUE;
		long avgTime = 0L;
		try (DB db = new DB("./target/nodes")) {
			int amount = 1000;
			String keyname = "keyname";
			System.out.println("update test");

			db.insert(new Doc(keyname, "n1").put("bool", false));

			db.updateOne(eq(keyname, "n1"), new Doc("$set", new Doc("bool", true)));
			Doc doc = db.findOne(eq(keyname, "n1"));
			System.out.println("found:");

			System.out.println(doc.toString());
		}

	}

	public void test4deleting() throws IOException, Exception {

		long maxTime = 0L;
		long minTime = Long.MAX_VALUE;
		long avgTime = 0L;
		try (DB db = new DB("./target/nodes")) {
			int amount = 1000;
			String keyname = "keyname";
			System.out.println("update test");

			long lastt = System.currentTimeMillis();
			System.out.println();
			for (int i = 0; i < amount; i++) {
				if (System.currentTimeMillis() - lastt > 10000) {
					System.out.println((i * 100) / amount + " %");
					lastt = System.currentTimeMillis();
				}
				Doc doc = genRandomDoc().put(keyname, i + "todelete");
				db.insert(doc);
			}
			System.out.println();
			lastt = System.currentTimeMillis();
			for (int i = 0; i < amount; i++) {
				if (System.currentTimeMillis() - lastt > 10000) {
					System.out.println((i * 100) / amount + " %");
					lastt = System.currentTimeMillis();
				}
				Doc q = new Doc(keyname, i + "todelete");
				if (i == 5) {
					getClass();
				}
				Doc doc = db.findOne(q);
				//assertNotNull(doc);
			}
			System.out.println();

			lastt = System.currentTimeMillis();
			for (int i = 0; i < amount; i++) {
				if (System.currentTimeMillis() - lastt > 10000) {
					System.out.println((i * 100) / amount + " %");
					lastt = System.currentTimeMillis();
				}
				Doc q = new Doc(keyname, i + "todelete");

				db.deleteOne(q);
			}

			System.out.println();
			lastt = System.currentTimeMillis();
			for (int i = 0; i < amount; i++) {
				if (System.currentTimeMillis() - lastt > 10000) {
					System.out.println((i * 100) / amount + " %");
					lastt = System.currentTimeMillis();
				}
				Doc q = new Doc(keyname, i + "todelete");
				Doc doc = db.findOne(q);
				assertNull(doc);
			}
			System.out.println();

			db.insert(new Doc(keyname, "n1").put("bool", false));
			db.insert(new Doc(keyname, "n12").put("bool", false));
			db.insert(new Doc(keyname, "n123").put("bool", false));
			db.insert(new Doc(keyname, "n1234").put("bool", false));
			//db.deleteOne(new Doc(keyname, "n1"));
			db.deleteOne(new Doc(keyname, "n12"));
			db.deleteOne(new Doc(keyname, "n123"));
			db.deleteOne(new Doc(keyname, "n1234"));

			db.updateOne(eq(keyname, "n1"), new Doc("$set", new Doc("bool", true)));
			Doc doc = db.findOne(eq(keyname, "n1"));
			System.out.println("found:");

			System.out.println(doc.toString());
			db.check();
		}

	}

	public void test5Intensive() throws IOException, Exception {

		long maxTime = 0L;
		long minTime = Long.MAX_VALUE;
		long avgTime = 0L;
		try (DB db = new DB("./target/nodes")) {
			int amount = 3000;
			String keyname = "keyname";
			System.out.println("find all the docs with {" + keyname + ":{$gt:" + 42 + "}}");

			db.insert(new Doc(keyname, 42));

			Iterator<Doc> it = db.find(gt(keyname, 42), new Doc(keyname, -1));
			while (it.hasNext()) {
				System.out.println(it.next());
			}
		}

	}

	public void test6Infinity() throws IOException, Exception {
		//10 minutes
		long duration = 1000 * 60 * 10;

		//1 minute
		duration = 1000 * 60;
		// 30 minutes
		//duration = 1000 * 60 * 30;
		try (DB db = new DB("./target/nodes")) {
			int i = 0;
			long starttime = System.currentTimeMillis();
			long lastt = System.currentTimeMillis();

			while (true) {
				long currentDuration = System.currentTimeMillis() - starttime;
				if (currentDuration > duration) {
					break;
				}
				if (System.currentTimeMillis() - lastt > 10000) {
					System.out.println((currentDuration * 100L) / duration + " % (" + i + ")");
					lastt = System.currentTimeMillis();
				}
				i++;
				String key = "inf_" + i;
				Doc doc = genRandomDoc();
				doc.put("keyname", key);
				db.insert(doc);
				db.updateOne(eq("keyname", key), new Doc("$set", new Doc("updated", 42)));

				Doc doc2 = db.findOne(eq("keyname", key));
				assertNotNull(doc2);
				assertTrue(doc2.has("updated"));
				assertEquals(doc2.get("updated"), 42);
				if (i % 2 == 0) {
					db.deleteOne(eq("keyname", key));
					Doc doc3 = db.findOne(eq("keyname", key));
					assertNull(doc3);
				}
			}
			db.check();
			System.err.println("DEFRAG");
			db.defrag();
			System.err.println("DEFRAG DONE");
			Doc r = db.check();
			db.dumpHoles();
			System.out.println(r);

		}

	}

	public Doc genRandomDoc() {
		Doc jo = new Doc();
		int kn = r.nextInt(10) + 1;
		for (int i = 0; i < kn; i++) {
			switch (r.nextInt(8)) {
				case 0:
					jo.put("boolean", r.nextBoolean());
					break;
				case 1:
					jo.put("int", r.nextInt());
					break;
				case 2:
					jo.put("long", r.nextLong());
					break;
				case 3:
					jo.put("float", r.nextFloat());
					break;
				case 4:
					jo.put("double", r.nextDouble());
					break;
				case 5:
					jo.put("doc", new Doc("key", r.nextInt()));

					break;
				case 6:
					jo.put("bytearray", new byte[r.nextInt(16) + 1]);
					break;
				default:
					String keyname = UUID.randomUUID().toString();
					keyname = keyname.substring(r.nextInt(keyname.length() - 1));
					jo.put(keyname, r.nextLong() + "");
					break;

			}

		}
		return jo;
	}
}
