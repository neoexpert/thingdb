/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db;

import com.neoexpert.db.index.btree.*;
import java.io.*;
import java.util.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.junit.*;
import org.junit.runners.MethodSorters;

/**
 * Unit test for simple App.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BTreeDel
		extends TestCase {

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public BTreeDel(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(BTreeDel.class);
	}

	private final int amount = 500;

	public void test3BTreeDeleting() {
		try {
			Random r = new Random();
			int amount = 30;
			int keysize = 8;
			File f = new File("./target/_btree");
			if (f.exists()) {
				assertTrue(f.delete());
			}
			BTreeIndex b = BTreeIndex.create(f, keysize, false);

			//for(int i=0;i<1000;i++)
			//	b.add(i,i);
			for (int i = 0; i < 1000; i++) {
				b.add(42, i);
			}

			//for(int i=0;i<1000;i++)
			//	b.add(i,i);
			int todel = new Random().nextInt(1000);

			for (int i = 0; i < 100; i++) {
				//todel = 4;
				b.print(System.out);
				b.delete(42, todel);
				System.out.println();
				b.print(System.out);
				System.out.println();

				System.out.println("HHHHHHHHHHHHHHH");
				Iterator<BTreeIndex.Entry> it = b.findGT(42,false);
				while (it.hasNext()) {
					BTreeIndex.Entry e = it.next();
					System.out.println(e);
					assertTrue(e.value != todel);

				}
				System.out.println("HHHHHHHHHHHHHHH");
			}

		} catch (Exception e) {
			e.printStackTrace(System.err);
			fail();
		}
	}

}
