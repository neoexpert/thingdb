/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db;

import com.neoexpert.db.index.*;
import com.neoexpert.db.index.btree.*;
import java.io.*;
import java.util.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import neo.db.UniqueConstraintException;
import org.junit.*;
import org.junit.runners.MethodSorters;

/**
 * Unit test for simple App.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BTreeTest
		extends TestCase {

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public BTreeTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(BTreeTest.class);
	}

	private final int amount = 50;

	public void test0ATreeBasics() throws IOException, Exception {
		//BTree b = new BTree(new File("./nodes/holes"));
		//b.print(System.out);
		//b.dump(System.out);
		//b.check(false);
	}

	public void test0BTreeBasics() throws IOException, Exception {
		long seed = 0;
		for (int k = 0; k < 10; k++) {
			Random r = new Random();

			seed = r.nextLong();

			r = new Random(seed);
			long seed2 = r.nextLong();

			int keysize = 4;
			File f = new File("./target/_btree");
			if (f.exists()) {
				assertTrue(f.delete());
			}
			BTreeIndex b = BTreeIndex.create(f, keysize, false);
			f.delete();
			int count1 = 0;
			int count2 = 0;

			for (int i = 0; i < 50; i++) {
				if (r.nextBoolean()) {
					b.add(1, i);
					count1++;

				} else {
					b.add(2, i);
					count2++;
				}
				//b.printNodes(System.out);
				//System.out.println();
				//System.out.println();
			}
			b.check(false);
			b.dump(System.out);

			assertTrue("seed was " + seed, b.count(1) == count1);
			assertTrue("seed was " + seed, b.count(2) == count2);

		}
	}

	public void test1BTreeBasics() {
		long seed = 0;
		for (int k = 0; k < 100; k++) {
			try {
				Random r = new Random();

				//seed = r.nextLong();
				r = new Random(seed);
				long seed2 = r.nextLong();

				int amount = 15;
				int keysize = 32;
				File f = new File("./target/_btree");
				if (f.exists()) {
					assertTrue(f.delete());
				}
				BTreeIndex b = BTreeIndex.create(f, keysize, false);
				int c = b.count(0);

				r = new Random(seed);

				int a = 100;

				//System.out.println("adding " + a+" keys...");
				for (int i = 0; i < a; i++) {
					int rand = r.nextInt(a);
					b.add(rand +"", rand);
					assertTrue(b.contains(rand + ""));
				}
				r = new Random(seed);
				//b.add(1, 1);
				//b.delete(1);
				//System.out.println("get " + a+" keys...");
				for (int i = 0; i < a; i++) {
					assertTrue(b.contains(r.nextInt(a) + ""));
				}

				r = new Random(seed);
				Random ri = new Random(seed2);
				//System.out.println("add another " + a+" keys while removing all previously added keys...");
				for (int i = 0; i < a; i++) {
					int key = ri.nextInt(a);

					int keytodel = r.nextInt(a);

					b.add(key + "", key);

					if(i==2)
						getClass();
					assertTrue("index was " + i + " seed was: " + seed, b.delete(keytodel + "") == keytodel);

					try {
						b.dump(System.out);
					} catch (Exception e) {
						getClass();
					}
					//System.out.println("check BTree...");

				}

				ri = new Random(seed2);
				//System.out.println("removing all previously added keys...");
				for (int i = 0; i < a; i++) {

					int keytodel = ri.nextInt(a);

					assertTrue("index was " + i + " seed was: " + seed, b.delete(keytodel + "") == keytodel);
					//System.out.println("check BTree...");

				}
				b.check(false);

				//to enable animation replace "/" with "///" in the following line:
				/*
			for (int i = 0; i < amount; i++) {
				//b.add(r.nextInt(amount), i);
				b.add(i, i);
				Thread.sleep(100);

				System.out.print("\033[H\033[2J");
				System.out.flush();
				System.out.println("btree animation for " + amount + " keys...");

				b.print(System.out);

			}
			//*/
				//b.print(System.out);
			} catch (Exception e) {
				e.printStackTrace(System.err);
				fail("seed was " + seed);
			}
		}
	}

	public void test2BTreeBasics() {
		try {
			Random r = new Random();
			int amount = 30;
			int keysize = 8;
			File f = new File("./target/_btree");
			if (f.exists()) {
				assertTrue(f.delete());
			}
			BTreeIndex b = BTreeIndex.create(f, keysize, false);

			int a = 0;
			for (int i = a; i >= 0; i--) {
				b.add(i, i);
				b.check(false);

			}
			for (int i = a; i > 0; i--) {
				assertTrue(b.contains(i));
			}

			b.print(System.out);
			System.out.println();
			System.out.println();

			for (int i = a; i >= 0; i--) {
				if (i == 2) {
					getClass();
				}
				assertTrue(b.delete(i) == i);

				b.print(System.out);
				System.out.println();
				System.out.println();

				b.check(false);

				for (int j = a; j >= 0; j--) {
					if (j >= i) {
						assertFalse(b.contains(j));
					} else {
						assertTrue(b.contains(j));
					}
				}

			}

			//assertFalse(b.contains(2));
			//to enable animation replace "/" with "///" in the following line:
			/*
			for (int i = 0; i < amount; i++) {
				//b.add(r.nextInt(amount), i);
				b.add(i, i);
				Thread.sleep(100);

				System.out.print("\033[H\033[2J");
				System.out.flush();
				System.out.println("btree animation for " + amount + " keys...");

				b.print(System.out);

			}
			//*/
			//b.print(System.out);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			fail();
		}
	}

	public void test4BTreeBalance() {
		try {
			Random r = new Random();
			int a = 10;
			int keysize = 8;
			File f = new File("./target/_balancing");
			if (f.exists()) {
				assertTrue(f.delete());
			}
			BTreeIndex b = BTreeIndex.create(f, keysize, false);

			System.out.println("check balancing...");
			for (int i = a; i > -a; i--) {
				b.add(i, i);
				b.check(false);
			}
			for (int i = -a; i < a; i++) {
				b.add(i, i);
				b.check(false);
			}
			for (int i = -a; i < a; i++) {
				b.add(0, i);
				b.check(false);
			}
			for (int i = -a; i < a; i++) {
				b.add(1, i);
				b.check(false);
				b.add(-1, i);
				b.check(false);
			}
			for (int i = -a; i < a; i++) {
				b.add(r.nextInt(200) - 100, i);
				b.check(false);
			}
			for (int i = -a; i < a; i++) {
				b.add(r.nextInt(), i);
				b.check(false);
			}

		} catch (Exception e) {
			e.printStackTrace(System.err);
			fail();
		}
		System.out.println("done.");
	}

	/**
	 * Rigourous Test :-)
	 */
	public void test5BTree() {
		try {
			Random r = new Random();
			int keysize = 40;
			File f = new File("./target/_" + keysize + "btree");
			if (f.exists()) {
				assertTrue(f.delete());
			}
			BTreeIndex b = BTreeIndex.create(f, keysize, false);
			b.close();
			b = new BTreeIndex(f);

			long maxTime = 0L;
			long minTime = Long.MAX_VALUE;
			long avgTime = 0L;
			System.out.println("insert " + 2 * amount + " strings (" + keysize + " bytes)...");
			for (int i = 0; i < amount; i++) {

				byte[] key = UUID.randomUUID().toString().getBytes();
				long t = System.nanoTime();
				b.add(key, i);
				t = System.nanoTime() - t;
				if (maxTime < t) {
					maxTime = t;
				}
				if (minTime > t) {
					minTime = t;
				}

				int value = ((amount - i) + "neoexpert").hashCode();
				t = System.nanoTime();
				b.add(amount - i, value);
				t = System.nanoTime() - t;
				if (maxTime < t) {
					maxTime = t;
				}
				if (minTime > t) {
					minTime = t;
				}

			}

			System.out.println("max_inserting_time: " + maxTime / 1000000000.0 + " seconds");
			System.out.println("min_inserting_time: " + minTime / 1000000000.0 + " seconds");
			System.out.println();
			//b.print(System.out);
			System.out.println();

			//b.add(6);
			System.out.println("getting " + amount + " strings (" + keysize + " bytes)...");
			maxTime = 0L;
			minTime = Long.MAX_VALUE;

			for (int i = 0; i < amount; i++) {
				long t = System.nanoTime();
				boolean ci = b.contains(amount - i);
				t = System.nanoTime() - t;
				if (maxTime < t) {
					maxTime = t;
				}
				if (minTime > t) {
					minTime = t;
				}
				assertTrue(ci);

				Integer value = ((amount - i) + "neoexpert").hashCode();
				Integer v = b.get(amount - i);

				assertEquals(v, value);

			}
			System.out.println("max_finding_time: " + maxTime / 1000000000.0 + " seconds");
			System.out.println("min_finding_time: " + minTime / 1000000000.0 + " seconds");

			System.out.println("getting randomly " + amount + " strings (" + keysize + " bytes)...");
			maxTime = 0L;
			minTime = Long.MAX_VALUE;
			for (int i = 0; i < amount; i++) {
				int key = r.nextInt(amount) + 1;
				long t = System.nanoTime();
				boolean ci = b.contains(key);
				t = System.nanoTime() - t;
				if (maxTime < t) {
					maxTime = t;
				}
				if (minTime > t) {
					minTime = t;
				}
				//assertTrue(ci);
			}
			System.out.println("max_finding_time: " + maxTime / 1000000000.0 + " seconds");
			System.out.println("min_finding_time: " + minTime / 1000000000.0 + " seconds");
			System.out.println("done.");

			for (int i = 0; i < 1000; i++) {
				assertFalse(b.contains(r.nextInt(1000000) + 100000));
			}

		} catch (Exception e) {
			e.printStackTrace(System.err);
			fail();
		}
	}

	public void test5BTreeUnique() throws IOException {
		Random r = new Random();
		int amount = 10;
		int keysize = 8;
		File f = new File("./target/_btreeunique");
		if (f.exists()) {
			assertTrue(f.delete());
		}
		BTreeIndex b = BTreeIndex.create(f, keysize, true);

		for (int i = 0; i < amount; i++) {
			b.add(i, i);
		}
		boolean fail = true;
		try {
			b.add(1, 1);
		} catch (UniqueConstraintException e) {
			fail = false;
		}
		if (fail) {
			fail();
		}
		//exception.expect(BTree.UniqueConstraintException.class);

		for (int i = 0; i < amount; i++) {
			try {
				b.add(i, i);
			} catch (UniqueConstraintException e) {
				continue;
			}

			fail("UniqueConstraintException not thrown");
		}
	}

	//TODO: make asserts
	public void test6BTreeTraversion() throws IOException {
		Random r = new Random();
		int amount = 1000;
		int keysize = 8;
		File f = new File("./target/_traverse");
		if (f.exists()) {
			assertTrue(f.delete());
		}
		BTreeIndex b = BTreeIndex.create(f, keysize, false);
		/*
		System.out.println();
		System.out.println("adding " + amount + " keys...");

		for (int i = 0; i < amount; i++) {
			b.add(i, i);
		}
		System.out.println("find all the keys greater than " + amount / 2 + " ...");
		b.findGT(new Traverser() {
			private int counter = amount / 2 + 1;

			@Override
			public void found(int kv) {
				//System.out.println("key found: " + kv);
			}
		}, amount / 2);

		System.out.println("find all the keys lesser than " + amount / 2 + " ...");
		b.findLT(new Traverser() {
			private int counter = amount / 2 + 1;

			@Override
			public void found(int kv) {
				//	assertTrue(kv==++counter);
				//System.out.println("key found: " + kv);
			}
		}, amount / 2);

		
		for (int i = 0; i < amount; i++) {
			b.add(42, i);
		}
		 */
		b.add(42, 42);
		b.add(43, 43);
		b.add(44, 44);
		b.add(45, 45);
		b.print(System.out);
		System.out.println();
		b.add(46, 46);

		b.print(System.out);
		System.out.println("find all entries for key " + 42);
		Iterator<BTreeIndex.Entry> it = b.findGT(0, true);
		while (it.hasNext()) {
			//it.next();
			System.out.println(it.next());
		}
	}
}
