/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db;

import java.io.*;
import java.util.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.junit.*;
import org.junit.runners.*;

/**
 * Unit test for simple App.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DBSync
		extends TestCase {

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public DBSync(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(DBSync.class);
	}
	Random r = new Random();

	public void test0Sync() throws IOException, Exception {
		
		System.out.println("crash test...");
		DB db = new DB("./target/crashtest");
		db = new DB("./target/crashtest");
		db.insert(new Doc("key","value"));
		db = new DB("./target/crashtest");
		Doc d=db.findOne(new Doc("key","value"));

		//db.createIndex("key2");
	}
	
	public void test1Sync() throws IOException, Exception {
		System.out.println("sync test...");
		try (DB db = new DB("./target/nodes")) {
			db.check();
			db.createIndex("keyname");
			//int v = r.nextInt();

			for (int i = 0; i < 10000; i++) {
				System.err.println(i);
				db.check();

				int l = r.nextInt(100)+1;
				db.insert(new Doc("keyname", l+new String(new byte[l])));
				db.check();
				db.update(new Doc("keyname",  l+new String(new byte[l])), new Doc("$set", new Doc("key", "" + r.nextLong())));

				db.delete(new Doc("keyname",  l+new String(new byte[l])));
				//db.check();

			}

		}

		//db.createIndex("key2");
	}

}
