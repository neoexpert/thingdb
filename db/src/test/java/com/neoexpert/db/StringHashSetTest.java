/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db;

import com.neoexpert.db.storage.*;
import java.io.*;
import java.util.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class StringHashSetTest
		extends TestCase {

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public StringHashSetTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(StringHashSetTest.class);
	}

	/**
	 * Rigourous Test :-)
	 */
	public void testHashMap() {
		try {
			Random r = new Random();
			File file = new File("./target/stringhashset/");
			file.mkdirs();
			StringHashSet s = new StringHashSet(file,"test");
			int amount = 1000;
			System.out.println("add "+amount+" Strings...");
			for (int i = 1; i < amount; i++) {
				s.add(i+"");
			}
			System.out.println("get "+amount+" Strings and delete every second String...");
			for (int i = 1; i < amount; i++) {

				assertEquals(s.get((i+"").hashCode()), i+"");
				if(i%2==0)
					s.delete(i+"");
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
			fail();
		}
	}

}
