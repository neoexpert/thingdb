/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.neoexpert.db;

import java.io.*;
import java.util.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class Binary
		extends TestCase {

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public Binary(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(Binary.class);
	}

	/**
	 * Rigourous Test :-)
	 */
	public void testDoc() {
		try {
			Random r = new Random();
			File file = new File("./db/doc");
			RandomAccessFile f = new RandomAccessFile(file, "rw");
			Doc d = new Doc();
			d.put("lala", "huhu");
			d.put("43", "lala");
			d.put("44", 45);
			d.put("45", 123L);
			d.put("46", true);
			d.put("47", 0.5f);
			d.put("48", 0.5);
			d.put("49", "ääöülsadkfjsldfkjß");

			long offset=0;
			f.seek(offset);
			
			d.write(0,f);
			f.seek(offset);
			d = new Doc();
			d.read(0,f);
			assertEquals(d.get("lala"), "huhu");
			assertEquals(d.get("43"), "lala");
			assertEquals(d.get("44"), 45);
			assertEquals(d.get("45"), 123L);
			assertEquals(d.get("46"), true);
			assertEquals(d.get("47"), 0.5f);
			assertEquals(d.get("48"), 0.5);
			assertEquals(d.get("49"), "ääöülsadkfjsldfkjß");

			f.seek(offset);
			assertEquals(Doc.getValue(f,42), "huhu");
			f.seek(offset);
			assertEquals(Doc.getValue(f,43), "lala");
			f.seek(offset);
			assertEquals(Doc.getValue(f,44), 45);
			f.seek(offset);
			assertEquals(Doc.getValue(f,45), 123L);
			f.seek(offset);
			assertEquals(Doc.getValue(f,46), true);
			f.seek(offset);
			assertEquals(Doc.getValue(f,47), 0.5f);
			f.seek(offset);
			assertEquals(Doc.getValue(f,48), 0.5);
			f.seek(offset);
			assertEquals(Doc.getValue(f,49), "ääöülsadkfjsldfkjß");

		} catch (Exception e) {
			e.printStackTrace(System.err);
			fail();
		}
	}

}
