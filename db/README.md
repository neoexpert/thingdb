# thingdb

is an embeddable lightweight (~50 kb) document-oriented on disk database written in java.

thingdb operates only on the disk and does not use RAM for caching.

## Android

thingdb works also on Android :-)

## Usage

```java
import static com.neoexpert.Q.*;
import com.neoexpert.*

...

DB db = new DB("./db");
```
### Insert Doc
```java
Doc doc=new Doc();
doc.put("name","test");
doc.put("age",31);
db.insert(doc);
```
### findOne Doc

```java
doc=db.findOne(eq("name","test"));
```
### find Docs

```java

db.insert(new Doc().put("key",42).put("hello","world"));
db.insert(new Doc().put("key",42).put("huhu","lala"));

Iterator<Doc> it=db.find(eq("key",42));
while(it.hasNext()){
	Doc doc=it.next();
	System.out.println(doc);
}

```
### update Docs

```java
db.update(eq("key",42),new Doc("$set",new Doc("updated",System.currentTimeMillis())));
```
## Indexes
You can create indexes to increase query-performance for a field:

```java
db.createIndex("fieldname");

//unique Index
db.createIndex("unique_field",/*unique:*/ true);
Doc doc=new Doc("unique_field","unique_value");
db.insert(doc);
db.insert(doc); //throws UniqueConstraintException
```
## Limits
to increase performance and reduce disk usage thingdb does not use String keys to store its docs, however you can still use doc.put(String, Object) and later get the value with doc.get(String). To acomplish this thingdb uses the hashCode() of the String key as key.


## Authors
neoexpert

## License



