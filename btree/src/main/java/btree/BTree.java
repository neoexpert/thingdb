/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package btree;

import btree.iterators.BTreeEQIterator;
import btree.iterators.BTreeGTIterator;
import btree.iterators.BTreeIterator;
import btree.iterators.BTreeLTIterator;
import neo.db.UniqueConstraintException;
import neo.db.btree.AbstractBTree;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * BTree
 *
 *
 */
public class BTree extends AbstractBTree implements AutoCloseable {

	private static final ByteOrder BYTE_ORDER = ByteOrder.BIG_ENDIAN;

	protected static final int VERSION = 4;
	private static final int TREE_ORDER = 1;
	//Max amount of keys per Node:
	protected static final int NODE_SIZE = TREE_ORDER + 1;
	//Amount of pointers to children nodes:
	//pointer key pointer key pointer
	protected static final int NODE_POINTERS = NODE_SIZE + 1;
	RandomAccessFile f;

	private final int keySize;
	private final long node_pointers_offset;
	private final long node_bytes;
	protected static final int VALUE_SIZE = 4;

	private final ArrayList<Integer> holes = new ArrayList<>();

	/**
	 *
	 * @param file
	 * @param keysize
	 * @param unique
	 * @return
	 * @throws IOException
	 */
	public static BTree create(File file, int keysize, boolean unique) throws IOException {
		try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
			if (raf.length() != 0) {
				throw new RuntimeException("file is not empty!");
			}
			raf.writeInt(42);
			raf.writeInt(VERSION);
			raf.writeInt(keysize);
			raf.writeBoolean(unique);
			int node_bytes
					= 4
					+ 1
					+ (NODE_SIZE + 1) * keysize
					+ (NODE_SIZE + 1) * VALUE_SIZE
					+ NODE_POINTERS * 4;
			raf.setLength(node_bytes);
		}
		return new BTree(file);
	}
	private boolean unique;

	/**
	 *
	 * @param file
	 * @throws IOException
	 */
	public BTree(File file) throws IOException {
		if (!file.exists()) {
			create(file, 4, false);
		}
		f = new RandomAccessFile(file, "rw");
		f.seek(0);
		if (f.readInt() != 42) {
			throw new RuntimeException(file + ": wrong BTree file format!");
		}
		if (f.readInt() != VERSION) {
			throw new RuntimeException(file + ": wrong VERSION of BTree");
		}
		this.keySize = f.readInt();
		this.unique = f.readBoolean();
		node_bytes
				= 4
				+ 1
				+ (NODE_SIZE + 1) * keySize
				+ (NODE_SIZE + 1) * VALUE_SIZE
				+ NODE_POINTERS * 4;
		node_pointers_offset
				= //parent pos int (4 byte)
				4
				//node length 1 byte (max 255)
				+ 1
				+ keySize * (NODE_SIZE + 1)
				+ VALUE_SIZE * (NODE_SIZE + 1);
		if (f.length() % node_bytes != 0) {
			throw new RuntimeException("corrupt file");
		}
	}

	/**
	 *
	 * @throws IOException
	 */
	@Override
	public void close() throws IOException {
		f.close();
	}

	/**
	 *
	 * @return @throws IOException
	 */
	public FNode getRoot() throws IOException {
		if (f.length() <= node_bytes) {
			return null;
		}
		return readNode(1);
	}


	/**
	 *
	 * @param k
	 * @param value
	 * @throws IOException
	 */
	protected synchronized void addA(byte[] k, int value) throws IOException {
		if (k.length != keySize) {
			if(k.length>keySize){
				throw new RuntimeException("keySize was exceeded. keySize="+keySize+" ,k.length="+k.length);
			}
			byte[] tmp = new byte[keySize];
			System.arraycopy(k, 0, tmp, 0, Math.min(k.length, Math.min(keySize, k.length)));
			k = tmp;
		}
		FNode root = getRoot();
		if (root == null) {
			root = new FNode();
			root.parent = -1;
			root.pos = 1;
			writeNode(root);
			root = getRoot();
		}

		root.add(k, value);
		defrag();
	}

	/**
	 *
	 * @param fix
	 * @throws Exception
	 */
	public void check(boolean fix) throws Exception {
		maxdepth = 0;
		FNode root = getRoot();
		if (root != null) {
			check(0, root, fix);
		}

	}
	private int maxdepth = 0;

	/**
	 *
	 * @param depth
	 * @param root
	 * @param fix_when_posible
	 * @throws IOException
	 */
	private void check(int depth, FNode root, boolean fix_when_posible) throws IOException {
		if (root.hasChildren()) {
			for (int i = 0; i < root.size + 1; i++) {
				if (root.vs.get(i).compareTo(root.vs.get(i).key) > 0) {
					throw new RuntimeException("wrong distribution");
				}
				FNode child = root.getChild(i);
				if (child == null) {
					throw new RuntimeException("corrupt pointers of node " + root);
				}
				if (child.parent != root.pos) {
					if (fix_when_posible) {
						root.fixChildren();
					} else {
						throw new RuntimeException("wrong parent id from Node " + child.pos + "! should be " + root.pos + ", but was " + child.parent);
					}
				}

				check(depth + 1, child, fix_when_posible);

			}
		} else if (maxdepth == 0) {
			maxdepth = depth;
		} else if (maxdepth != depth) {
			throw new RuntimeException("is not balanced depths: " + maxdepth + " != " + depth);
		}

	}


	
	/**
	 *
	 * @param k
	 * @param val
	 * @return
	 * @throws IOException
	 */
	public Integer get(Object k, int val) throws IOException {
		//return get(Value.get(k).getBytes(),val);
		return null;
	}
	

	/**
	 *
	 * @param k
	 * @return
	 * @throws IOException
	 */
	public Integer get(byte[] k) throws IOException {
		if (k.length != keySize) {
			byte[] tmp = new byte[keySize];
			System.arraycopy(k, 0, tmp, 0, Math.min(keySize, k.length));
			k = tmp;
		}
		FNode root = getRoot();
		if (root == null) {
			return null;
		}
		return root.get(k);
	}
	
	/**
	 *
	 * @param k
	 * @param val
	 * @return
	 * @throws IOException
	 */
	public Integer get(byte[] k, int val) throws IOException {
		if (k.length != keySize) {
			byte[] tmp = new byte[keySize];
			System.arraycopy(k, 0, tmp, 0, Math.min(keySize, k.length));
			k = tmp;
		}
		FNode root = getRoot();
		if (root == null) {
			return null;
		}
		return root.get(k,val);
	}

	private void clear() throws IOException {
		f.setLength(node_bytes);
	}

	/**
	 *
	 * @param k
	 * @return
	 * @throws IOException
	 */
	public Integer delete(byte[] k) throws IOException {
		if (k.length != keySize) {
			byte[] tmp = new byte[keySize];
			System.arraycopy(k, 0, tmp, 0, Math.min(keySize, k.length));
			k = tmp;
		}
		FNode root = getRoot();
		if (root == null) {
			return null;
		}
		Integer v = root.delete(k);
		defrag();
		return v;
	}

	/**
	 *
	 * @param k
	 * @param val
	 * @return
	 * @throws IOException
	 */
	public Integer delete(byte[] k, int val) throws IOException {
		if (k.length != keySize) {
			byte[] tmp = new byte[keySize];
			System.arraycopy(k, 0, tmp, 0, Math.min(keySize, k.length));
			k = tmp;
		}
		FNode root = getRoot();
		if (root == null) {
			return null;
		}
		Integer v = root.delete(k, val);
		defrag();
		return v;
	}



	/**
	 *
	 * @param k
	 * @return
	 * @throws IOException
	 */
	public boolean containsA(byte[] k) throws IOException {
		if (k.length != keySize) {
			byte[] tmp = new byte[keySize];
			System.arraycopy(k, 0, tmp, 0, Math.min(keySize, k.length));
			k = tmp;
		}
		FNode root = getRoot();
		if (root == null) {
			return false;
		}
		boolean v = root.contains(k);
		return v;
	}

	/**
	 *
	 * @param k
	 * @return
	 * @throws IOException
	 */
	public boolean contains(Object k) throws IOException {
		//return containsA(Value.get(k).getBytes());
		return false;
	}

	private FNode readNode(int pos) throws IOException {
		f.seek(pos * node_bytes);
		FNode n = new FNode();
		n.parent = f.readInt();
		n.size = (byte) f.read();

		for (int i = 0; i < NODE_SIZE + 1; i++) {
			byte[] k = new byte[keySize];
			f.read(k);
			n.vs.add(new Entry(k, f.readInt()));
		}
		for (int i = 0; i < NODE_POINTERS; i++) {
			n.ps.add(f.readInt());
		}
		n.pos = pos;
		return n;
	}

	/**
	 *
	 * @param n
	 * @throws IOException
	 */
	public void deleteNode(FNode n) throws IOException {
		holes.add(n.pos);
		n.parent = -3;
		writeNode(n);
	}

	private void defrag() throws IOException {
		long len = f.length();
		int lnp = (int) (len / node_bytes) - 1;

		for (int i=0;i<holes.size();i++) {
			int hole=holes.get(i);
			FNode n = readNode(lnp);
			if (n.parent == -3) {
				continue;
			}
			n.pos = hole;
			writeNode(n);
			n.fixParent(lnp);
			n.fixChildren();
			lnp--;
		}
		holes.clear();
		f.setLength((lnp + 1) * node_bytes);
	}

	/**
	 *
	 * @param n
	 * @throws IOException
	 */
	public void writeNode(FNode n) throws IOException {

		f.seek(n.pos * node_bytes);
		f.writeInt(n.parent);
		f.write(n.size);
		for (int i = 0; i < n.size; i++) {
			f.write(n.vs.get(i).key, 0, keySize);
			f.writeInt(n.vs.get(i).value);
		}
		f.seek(n.pos * node_bytes + node_pointers_offset);
		for (int i = 0; i < NODE_POINTERS; i++) {
			if (i < n.ps.size()) {
				f.writeInt(n.ps.get(i));
			} else {
				f.writeInt(0);
			}
		}
		//n.parent=-1;
		//n.vs.clear();
		//n.ps.clear();
		//n.size=-1;
	}

	private FNode appendNode(FNode n) throws IOException {
		long len = f.length();
		if (n.parent > 2) {
			long lnp = len / node_bytes - 1;
			FNode ln = readNode((int) lnp);
			if (ln.parent == -2) {
				n.pos = ln.ps.get(0);
				System.out.println("add at " + n.pos);
				f.setLength(lnp * node_bytes);
			} else {
				n.pos = (int) (len / node_bytes);
			}
		} else {
			n.pos = (int) (len / node_bytes);
		}

		writeNode(n);
		return n;
	}

	/**
	 *
	 * @param out
	 * @throws IOException
	 */
	public void dump(PrintStream out) throws IOException {
		long len = f.length();
		int lnp = (int) (len / node_bytes);
		for (int i = 0; i < lnp; i++) {
			FNode n = readNode(i);
			if(n.parent==-3)
				throw new RuntimeException("deleted node found");
			//out.println(n);
		}
	}

	/**
	 *
	 * @param out
	 * @throws IOException
	 */
	public void print(PrintStream out) throws IOException {
		FNode root = getRoot();
		if (root != null) {
			root.print(out);
		}
	}

	/**
	 *
	 * @param out
	 * @throws IOException
	 */
	public void printNodes(PrintStream out) throws IOException {
		FNode root = getRoot();
		if (root != null) {
			root.printNodes(out, "");
		}
	}


	/**
	 *
	 * @return
	 * @throws IOException
	 */
	public Iterator<Entry> find() throws IOException {
		FNode root = getRoot();
		if (root != null) {
			return new BTreeIterator(root);
		}
		return null;
	}





	/**
	 *
	 * @param k
	 * @return
	 * @throws IOException
	 */
	public Iterator<Entry> find(byte[] k) throws IOException {
		if (k.length != keySize) {
			byte[] tmp = new byte[keySize];
			System.arraycopy(k, 0, tmp, 0, Math.min(keySize, k.length));
			k = tmp;
		}
		FNode root = getRoot();
		if (root != null) {
			return new BTreeEQIterator(root, k);
		}
		return null;
	}

	/**
	 *
	 * @param k
	 * @param desc
	 * @return
	 * @throws IOException
	 */
	public Iterator<Entry> findGT(byte[] k, boolean desc) throws IOException {
		if (k.length != keySize) {
			byte[] tmp = new byte[keySize];
			System.arraycopy(k, 0, tmp, 0, Math.min(keySize, k.length));
			k = tmp;
		}
		FNode root = getRoot();
		if (root != null) {
			return new BTreeGTIterator(root, k, desc);
		}
		return null;
	}

	/**
	 *
	 * @param k
	 * @param desc
	 * @return
	 * @throws IOException
	 */
	public Iterator<Entry> findLT(byte[] k, boolean desc) throws IOException {
		if (k.length != keySize) {
			byte[] tmp = new byte[keySize];
			System.arraycopy(k, 0, tmp, 0, Math.min(keySize, k.length));
			k = tmp;
		}
		FNode root = getRoot();
		if (root != null) {
			return new BTreeLTIterator(root, k, desc);
		}
		return null;
	}

	/**
	 *
	 * @param t
	 * @param k
	 * @throws IOException
	 */
	public void traverseGT(Traverser t, byte[] k) throws IOException {
		FNode root = getRoot();
		if (root != null) {
			root.traverseGT(t, k);
		}
	}

	/**
	 *
	 * @param t
	 * @param k
	 * @throws IOException
	 */
	public void findLT(Traverser t, byte[] k) throws IOException {
		FNode root = getRoot();
		if (root != null) {
			root.findLT(t, k);
		}
	}

	/**
	 *
	 * @param key
	 * @return
	 * @throws IOException
	 */
	public int count(byte[] key) throws IOException {
		int c = 0;
		Iterator<Entry> it = find(key);
		if (it == null) {
			return 0;
		}
		while (it.hasNext()) {
			c++;
			it.next();
		}
		return c;
	}

	

	/**
	 *
	 */
	public class Entry {

		/**
		 *
		 * @param k
		 * @param v
		 */
		public Entry(byte[] k, int v) {
			this.key = k;
			this.value = v;
		}

		/**
		 *
		 */
		public byte[] key;

		/**
		 *
		 */
		public int value;

		/**
		 *
		 * @param k
		 * @return
		 */
		public int compareTo(byte[] k) {
			for (int i = 0, j = 0; i < k.length && j < key.length; i++, j++) {
				int a = (k[i] & 0xff);
				int b = (key[j] & 0xff);
				if (a != b) {
					return a - b;
				}
			}
			return k.length - key.length;
		}

		/**
		 *
		 * @param k
		 * @return
		 */
		public int compareuniqueTo(byte[] k) {
			for (int i = 0, j = 0; i < k.length && j < key.length; i++, j++) {
				int a = (k[i] & 0xff);
				int b = (key[j] & 0xff);
				if (a != b) {
					return a - b;
				}
			}
			if (unique) {
				if (k.length == key.length) {
					throw new UniqueConstraintException();
				}
			}
			return k.length - key.length;
		}

		/**
		 *
		 * @return
		 */
		@Override
		public String toString() {
			return "(" + key.toString() + "," + value + ")";
		}

		
		/**
		 *
		 * @return
		 */
		public int getKeyAsInt() {
			return ByteBuffer.wrap(key).order(BYTE_ORDER).getInt();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Entry) {
				Entry e = (Entry) obj;
				if (e.value != this.value) {
					return false;
				}
				return compareTo(e.key) == 0;
			}
			return false;
		}

	}

	/**
	 *
	 */
	public class FNode{

		int parent;

		/**
		 *
		 */
		public byte size;
		int pos;
		//values

		/**
		 *
		 */
		public ArrayList<Entry> vs = new ArrayList<>();
		//children-pointers
		ArrayList<Integer> ps = new ArrayList<>();

		/**
		 *
		 * @return @throws IOException
		 */
		public FNode getParent() throws IOException {
			if (parent == -1) {
				return null;
			}
			return readNode(parent);
		}

		/**
		 *
		 * @param pos
		 * @return
		 * @throws IOException
		 */
		public FNode getChild(int pos) throws IOException {
			if (pos >= ps.size() || pos < 0) {
				return null;
			}
			int addr = ps.get(pos);
			if (addr == 0) {
				return null;
			}
			return readNode(addr);
		}

		/**
		 *
		 * @param pos
		 * @return
		 * @throws IOException
		 */
		public FNode getLeftChild(int pos) throws IOException {
			return getChild(pos);
		}

		/**
		 *
		 * @param pos
		 * @return
		 * @throws IOException
		 */
		public FNode getRightChild(int pos) throws IOException {
			return getChild(pos + 1);
		}

		private int checkFor(byte[] v) {
			int p;
			for (p = 0; p < size; p++) {
				Entry iv = vs.get(p);

				if (iv.compareTo(v) <= 0) {
					break;
				}
			}
			return p;
		}

		private int checkForUnique(byte[] v) {
			int p;
			for (p = 0; p < size; p++) {
				Entry iv = vs.get(p);

				if (iv.compareuniqueTo(v) <= 0) {
					break;
				}
			}
			return p;
		}

		/**
		 *
		 * @return
		 */
		protected boolean isFull() {
			return size > NODE_SIZE;
		}

		private FNode getBestReplacementFor(int pos) throws IOException {
			FNode c = getChild(pos);
			while (c != null) {
				FNode lc = c.getLastChild();
				if (lc == null) {
					break;
				}
				c = lc;
			}
			return c;
		}

		private FNode getLastChild() throws IOException {
			return getChild(size);
		}

		private Integer delete(byte[] k, int val) throws IOException {
			int p = checkFor(k);
			int i = 0;
			while (p + i < size) {
				Entry entry = vs.get(p + i);
				if (entry.compareTo(k) == 0) {
					if (entry.value != val) {
						i++;
						FNode child = getChild(p + i);
						if (child != null) {
							Integer v = child.delete(k, val);
							if (v != null) {
								return v;
							}
						}
						continue;
					}

					Entry kv = vs.remove(p + i);
					size--;
					if (isLeaf()) {
						balance(kv);

						return kv.value;
					} else {
						FNode r = getBestReplacementFor(p + i);
						Entry rkv = r.vs.remove(r.size - 1);
						r.size--;
						vs.add(p + i, rkv);
						size++;
						writeNode(this);
						writeNode(r);

						r.balance(rkv);
						return kv.value;
					}

				} else {
					break;
				}
			}
			FNode child = getChild(p);
			if (child != null) {
				return child.delete(k, val);
			}
			return null;
		}

		/**
		 *
		 * @param k
		 * @return
		 * @throws IOException
		 */
		public Integer delete(byte[] k) throws IOException {
			int p = checkFor(k);
			if (p < size) {
				if (vs.get(p).compareTo(k) == 0) {

					Entry kv = vs.remove(p);
					size--;
					if (isLeaf()) {
						balance(kv);

						return kv.value;
					} else {
						FNode r = getBestReplacementFor(p);
						Entry rkv = r.vs.remove(r.size - 1);
						r.size--;
						vs.add(p, rkv);
						size++;
						writeNode(this);
						writeNode(r);

						r.balance(rkv);
						return kv.value;
					}

				}
			}
			FNode child = getChild(p);
			if (child != null) {
				return child.delete(k);
			}
			return null;
		}

		/**
		 *
		 * @param k
		 * @return
		 * @throws IOException
		 */
		public Integer get(byte[] k) throws IOException {
			int p = checkFor(k);
			if (p < vs.size()) {
				if (vs.get(p).compareTo(k) == 0) {
					return vs.get(p).value;
				}
			}
			FNode child = getChild(p);
			if (child != null) {
				return child.get(k);
			}
			return null;
		}

		/**
		 *
		 * @param k
		 * @param val
		 * @return
		 * @throws IOException
		 */
		public Integer get(byte[] k, int val) throws IOException {
			int p = checkFor(k);
			int i = 0;
			while (p + i < size) {
				Entry entry = vs.get(p + i);
				if (entry.compareTo(k) == 0) {
					if (entry.value != val) {
						i++;
						FNode child = getChild(p + i);
						if (child != null) {
							Integer v = child.get(k, val);
							if (v != null) {
								return v;
							}
						}
					}

				} else {
					break;
				}
			}
			FNode child = getChild(p);
			if (child != null) {
				return child.get(k, val);
			}
			return null;
		}

		/**
		 *
		 * @param k
		 * @return
		 * @throws IOException
		 */
		public boolean contains(byte[] k) throws IOException {
			int p = checkFor(k);
			if (p < size) {
				if (vs.get(p).compareTo(k) == 0) {
					return true;
				}
			}
			FNode child = getChild(p);
			if (child != null) {
				return child.contains(k);
			}
			return false;
		}

		/**
		 *
		 * @param kv
		 * @return
		 */
		protected int insert(Entry kv) {
			int p = checkFor(kv.key);
			vs.add(p, kv);
			size++;
			return p;
		}

		/**
		 *
		 * @param k
		 * @param v
		 * @throws IOException
		 */
		public void add(byte[] k, int v) throws IOException {
			int bpos = checkForUnique(k);
			FNode child = getChild(bpos);
			if (child != null) {
				child.add(k, v);
			} else {
				vs.add(bpos, new Entry(k, v));
				size++;
				if (isFull()) {
					split();
				} else {
					writeNode(this);
				}
			}
		}

		/**
		 *
		 * @param k
		 * @throws IOException
		 */
		public void balance(Entry k) throws IOException {
			if (size == 0) {
				balanceLeaf(k);
			} else {
				writeNode(this);
			}
		}

		/**
		 *
		 * @param k
		 * @throws IOException
		 */
		public void balanceLeaf(Entry k) throws IOException {
			FNode p = getParent();
			if (p == null) {
				FNode child = getChild(0);
				if (child == null) {
					clear();
					return;
				}
				deleteNode(child);
				child.pos = 1;
				child.parent = -1;
				child.fixChildren();
				writeNode(child);

				return;
			}

			int pp = p.getPointerPosition(this.pos);
			FNode sr = p.getChild(pp + 1);
			FNode sl = p.getChild(pp - 1);
			//TODO:check that:
			if (sr != null) {
				if (sr.size == 2) {
					Entry tp = sr.vs.remove(0);
					sr.size--;
					p.vs.add(pp, tp);
					tp = p.vs.remove(pp + 1);
					vs.add(size, tp);
					size++;
					Integer pointer = sr.ps.remove(0);

					if (pointer != 0) {
						FNode srleftnode = readNode(pointer);
						srleftnode.parent = pos;
						writeNode(srleftnode);
					}

					ps.add(size, pointer);

					writeNode(p);
					writeNode(sr);
					writeNode(this);
					return;
				}
			}
			if (sl != null) {
				if (sl.size == 2) {
					Entry tp = sl.vs.remove(sl.size - 1);
					sl.size--;
					p.vs.add(pp - 1, tp);
					tp = p.vs.remove(pp);
					vs.add(size, tp);
					size++;
					Integer pointer = sl.ps.remove(sl.size + 1);
					if (pointer != 0) {
						FNode srrightnode = readNode(pointer);
						srrightnode.parent = pos;
						writeNode(srrightnode);
						ps.add(0, pointer);
					}

					writeNode(p);
					writeNode(sl);
					writeNode(this);
					return;
				}
			}

			if (sr != null && sl == null) {
				Entry kv = p.vs.remove(pp);
				p.size--;
				//Integer pointer = 
				p.ps.remove(pp + 1);
				vs.add(size, kv);
				size++;
				ps.add(size, sr.ps.remove(0));

				pp = sr.checkFor(kv.key);
				kv = sr.vs.remove(pp);
				sr.size--;
				vs.add(size, kv);
				size++;
				ps.add(size, sr.ps.remove(0));

				writeNode(p);

				deleteNode(sr);
				writeNode(this);
				fixChildren();

				if (p.isEmpty()) {
					p.balance(k);
					return;

				} else {
					writeNode(this);
				}
				return;
			}
			if (sl != null && sr == null) {
				Entry kv = p.vs.remove(pp - 1);
				p.size--;
				//Integer pointer = 
				p.ps.remove(pp - 1);
				vs.add(0, kv);
				size++;
				ps.add(0, sl.ps.remove(sl.size));

				//pp = sl.checkFor(kv.key);
				kv = sl.vs.remove(0);
				sl.size--;
				vs.add(0, kv);
				size++;
				ps.add(0, sl.ps.remove(sl.size));

				writeNode(p);

				deleteNode(sl);
				writeNode(this);
				fixChildren();

				if (p.isEmpty()) {
					p.balance(k);

				} else {
					writeNode(this);
				}
				return;
			}
			if (sr != null && sl != null) {
				Entry kv = p.vs.remove(0);
				p.ps.remove(1);
				p.size--;
				sl.vs.add(sl.size, kv);
				sl.size++;
				sl.ps.add(sl.size, ps.remove(0));
				sl.fixChildren();
				writeNode(sl);
				writeNode(p);
				deleteNode(this);
			}
		}

		/**
		 *
		 * @throws IOException
		 */
		public void split() throws IOException {
			FNode p = getParent();

			if (p == null) {
				p = new FNode();
				p.parent = -1;
				p.pos = 1;
			}
			FNode cl = new FNode();
			FNode cr = new FNode();
			for (int i = 0; i < NODE_SIZE / 2; i++) {
				cl.insert(vs.remove(i));
				size--;
				if (!ps.isEmpty()) {
					cl.ps.add(ps.remove(0));
				}
				if (!ps.isEmpty()) {
					cl.ps.add(ps.remove(0));
				}

			}
			for (; size > 1; ) {
				cr.insert(vs.remove(1));
				size--;
				if (!ps.isEmpty()) {
					cr.ps.add(ps.remove(0));
				}
				if (!ps.isEmpty()) {
					cr.ps.add(ps.remove(0));
				}
			}
			int ppos = p.insert(vs.get(0));
			if (!p.ps.isEmpty()) {
				ppos = p.ps.indexOf(pos);
				p.ps.remove(ppos);
			}

			cl.parent = p.pos;
			cr.parent = p.pos;

			if (pos != 1) {
				cl.pos = pos;
				writeNode(cl);
			} else {
				appendNode(cl);
			}

			p.ps.add(ppos++, cl.pos);

			appendNode(cr);
			cl.fixChildren();
			cr.fixChildren();
			p.ps.add(ppos, cr.pos);

			writeNode(p);
			if (p.isFull()) {
				p.split();
			}

		}

		private void _findEQ(Traverser t, byte[] k) throws IOException {
			for (int i = 0; i < size; i++) {
				Entry kv = vs.get(i);
				int c = kv.compareTo(k);
				if (c < 0) {
					continue;
				}
				if (c >= 0) {
					FNode child = getChild(i);
					if (child != null) {
						child._findEQ(t, k);
					}
				} else {
					FNode child = getChild(i + 1);
					if (child != null) {
						child._findEQ(t, k);
					}
				}
				if (c == 0) {
					t.found(kv.value);
				}
			}
			FNode child = getChild(size);
			if (child != null) {
				child._findEQ(t, k);
			}
		}

		private void findLT(Traverser t, byte[] k) throws IOException {
			for (int i = 0; i < size; i++) {
				Entry kv = vs.get(i);
				int c = kv.compareTo(k);
				if (c >= 0) {
					continue;
				}
				if (c < 0) {
					FNode child = getChild(i);
					if (child != null) {
						child.findLT(t, k);
					}
				} else {
					FNode child = getChild(i + 1);
					if (child != null) {
						child.findLT(t, k);
					}
				}
				t.found(kv.value);
			}
			FNode child = getChild(size);
			if (child != null) {
				child.findLT(t, k);
			}
		}

		private void traverseGT(Traverser t, byte[] k) throws IOException {
			for (int i = 0; i < size; i++) {
				Entry kv = vs.get(i);
				int c = kv.compareTo(k);
				if (c <= 0) {
					continue;
				}
				if (c > 0) {
					FNode child = getChild(i);
					if (child != null) {
						child.traverseGT(t, k);
					}
				} else {
					FNode child = getChild(i + 1);
					if (child != null) {
						child.traverseGT(t, k);
					}
				}
				t.found(kv.value);
			}
			FNode child = getChild(size);
			if (child != null) {
				child.traverseGT(t, k);
			}
		}

		/**
		 *
		 * @param out
		 * @param e
		 * @throws IOException
		 */
		public void printNodes(PrintStream out, String e) throws IOException {
			out.println(e + this);
			if (isLeaf()) {
				return;
			}
			for (int i = 0; i <= size; i++) {
				FNode c = getChild(i);
				c.printNodes(out, e + " ");
			}
		}

		/**
		 *
		 * @param out
		 * @throws IOException
		 */
		public void print(PrintStream out) throws IOException {
			print(out, "", "─");
		}

		private void print(PrintStream out, String e, String lr) throws IOException {
			int i = 0;
			for (; i < size; i++) {
				FNode c = getChild(i);
				if (c == null) {
					out.println(e + lr + vs.get(i).toString());

					continue;
				}
				if (i == 0) {
					c.print(out, e + " ", "┌");
				} else {
					c.print(out, e + " ", "└");
				}

				out.println(e + lr + vs.get(i).toString());
			}
			FNode c = getChild(i);
			if (c != null) {
				c.print(out, e + " ", "└");
			}
		}

		/**
		 *
		 * @return
		 */
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(pos);
			sb.append(" :");

			sb.append("{");
			sb.append(parent);
			sb.append(",");
			sb.append(size);
			sb.append("}");
			sb.append("{");
			for (int i = 0; i < size; i++) {
				sb.append(vs.get(i).toString());
			}
			sb.append("}");
			sb.append("{");

			for (Integer v : ps) {
				sb.append(v)
						.append(",");
			}
			sb.append("}");
			return sb.toString();
		}

		private boolean fixParent(int oldpos) throws IOException {
			FNode p;
			try {
				p = getParent();
			} catch (EOFException e) {
				return false;
			}
			for (int i = 0; i < p.ps.size(); i++) {
				if (p.ps.get(i) == oldpos) {
					p.ps.set(i, pos);
					writeNode(p);
					return true;
				}
			}
			return false;
		}

		private void fixChildren() throws IOException {
			for (int i = 0; i < size + 1; i++) {
				FNode c = getChild(i);
				if (c == null) {
					return;
				}
				c.parent = pos;
				writeNode(c);
			}
		}

		/**
		 *
		 * @return
		 */
		public boolean isLeaf() {
			return !hasChildren();
		}

		/**
		 *
		 * @return
		 */
		public boolean hasChildren() {
			if (!ps.isEmpty()) {
				return ps.get(0) != 0;
			}
			return false;
		}

		private boolean isEmpty() {
			return size == 0;
		}

		private int getPointerPosition(int pos) {
			for (int i = 0; i < size + 1; i++) {
				if (ps.get(i) == pos) {
					return i;
				}
			}
			return -1;
		}

	}
	private final static char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();

	/**
	 *
	 * @param bytes
	 * @return
	 */
	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = HEX_CHARS[v >>> 4];
			hexChars[j * 2 + 1] = HEX_CHARS[v & 0x0F];
		}
		return new String(hexChars);
	}
}
