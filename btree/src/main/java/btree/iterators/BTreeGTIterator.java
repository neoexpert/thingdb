/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package btree.iterators;


import btree.BTree;

import java.io.IOException;
import java.util.Iterator;
import java.util.Stack;

/**
 *
 * @author neoexpert
 */
public class BTreeGTIterator implements Iterator<BTree.Entry> {

	private final byte[] k;
	boolean desc = true;

	/**
	 *
	 * @param r
	 * @param k
	 * @param desc
	 * @throws IOException
	 */
	public BTreeGTIterator(BTree.FNode r, byte[] k, boolean desc) throws IOException {
		this.desc = desc;
		this.k = k;
		if (desc) {
			s.push(new State(r, r.size - 1));
		} else {
			s.push(new State(r, 0));
		}
		getNext();

	}

	Stack<State> s = new Stack<>();

	BTree.Entry next = null;

	/**
	 *
	 * @throws IOException
	 */
	public final void getNext() throws IOException {
		if (desc) {
			next = finddesc();
		} else {
			next = find();
		}
	}

	/**
	 *
	 * @return
	 */
	@Override
	public boolean hasNext() {
		return next != null;
	}


	/**
	 *
	 * @return
	 */
	@Override
	public BTree.Entry next() {
		BTree.Entry current = next;
		try {
			getNext();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return current;
	}

	@Override
	public void remove() {

	}

	private BTree.Entry find() throws IOException {
		loop:
		while (!s.isEmpty()) {
			State st = s.peek();
			for (; st.i < st.n.size;) {
				switch (st.pos) {
					case State.CHECK_VALUE:
						st.kv = st.n.vs.get(st.i);
						st.c = st.kv.compareTo(k);

						if (st.c <= 0) {
							BTree.FNode child = st.n.getLeftChild(st.i);
							st.pos = State.RETURN;

							if (child != null) {
								s.push(new State(child, 0));
								continue loop;
							}

						} else {
							st.pos = State.INCREMENT;
						}
						break;
					case State.RETURN:
						st.pos = State.INCREMENT;

						if (st.c <= 0) {
							return st.kv;
						}
						break;
					case State.INCREMENT:

						st.i++;
						st.pos = State.CHECK_VALUE;

						if (st.i == st.n.size) {
							BTree.FNode child = st.n.getRightChild(st.i - 1);
							if (child != null) {
								s.push(new State(child, 0));
								continue loop;
							}
						}
						break;
				}

			}
			s.pop();
		}
		return null;

	}

	private BTree.Entry finddesc() throws IOException {
		loop:
		while (!s.isEmpty()) {
			State st = s.peek();
			for (; st.i >= 0;) {
				switch (st.pos) {
					case State.CHECK_VALUE:
						st.kv = st.n.vs.get(st.i);
						st.c = st.kv.compareTo(k);

						if (st.c <= 0) {
							BTree.FNode child = st.n.getRightChild(st.i);
							st.pos = State.RETURN;

							if (child != null) {
								s.push(new State(child, child.size - 1));
								continue loop;
							}

						} else {
							st.pos = State.INCREMENT;
						}
						break;
					case State.RETURN:
						st.pos = State.INCREMENT;

						if (st.c <= 0) {
							return st.kv;
						}
						break;
					case State.INCREMENT:

						st.i--;
						st.pos = State.CHECK_VALUE;

						if (st.i == -1) {
							BTree.FNode child = st.n.getLeftChild(st.i + 1);
							if (child != null) {
								s.push(new State(child, child.size - 1));
								continue loop;
							}
						}
						break;
				}

			}
			s.pop();
		}
		return null;

	}
}
