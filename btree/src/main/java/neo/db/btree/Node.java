package neo.db.btree;

public abstract class Node<T extends Comparable>{
	protected final ABTree tree;
	public Node(ABTree tree){
		this.tree=tree;
	}
	public abstract int size();
	public abstract T getKey(int index);
	public abstract void setKey(int index, T key);
	public abstract void insert(int index, T key);
	public abstract int insert(T key);
	public abstract Node getChildNode(int index);
	public abstract Node createChildNode(int index, int size, T[] keys, Node[] pointers);
	public abstract Node getParent();
	public boolean isFull(){
		return tree.maxSize==size();
	}

	public void add(T key){
		int max=Math.min(tree.maxSize, size());
		int i=0;
		for(;i<max;++i){
			if(key.compareTo(getKey(i))<=0){
				Node left=getChildNode(i);
				if(left!=null){
					left.add(key);
					return;
				}
				break;
			}
		}
		Node right = getChildNode(i);
		if(right!=null){
			right.add(key);
			return;
		}
		insert(i, key);
		if(isFull()){
			split();
		}
	}

	protected Node<T> split(){
		Node parent=getParent();
		//if(parent==null)
		//	parent=createRoot();
		int halfSize=tree.halfSize;
		int maxSize=tree.maxSize;
		T[] left=(T[])new Comparable[maxSize];
		T[] right=(T[])new Comparable[maxSize];
		Node[] pointersLeft=new Node[maxSize+1];
		Node[] pointersRight=new Node[maxSize+1];
		T median=getKey(halfSize);
		for(int i=0;i<halfSize;++i){
			left[i]=getKey(i);
			pointersLeft[i]=getChildNode(i);
		}
		pointersLeft[halfSize]=getChildNode(halfSize);
		int rOffset=halfSize+1;
		for(int i=0;i<halfSize;++i){
			right[i]=getKey(rOffset+i);
			pointersRight[i]=getChildNode(rOffset+i);
		}
		pointersRight[halfSize]=getChildNode(halfSize+rOffset);
		int index=parent.insert(median);
		parent.deletePoiner(index);
		parent.createChildNode(index, halfSize, left, pointersLeft);
		parent.createChildNode(index+1, halfSize, right, pointersRight);
		if(parent.isFull()) {
			Node<T> newParent = parent.split();
			//if(newParent!=parent)
			//	newParent.fixChildren();
		}
		return parent;
	}

	protected abstract void deletePoiner(int index);

	protected abstract void removeChild(Node<T> tNode);

	private void fixChildren() {
		int size = size()+1;
		for(int i=0;i<size;++i)
			getChildNode(i).setParent(this);
	}

	protected abstract void setParent(Node<T> tNode);

	public boolean containsKey(T key) {
		int size=size();
		int i=0;
		for(;i<size;++i){
			int result = key.compareTo(getKey(i));
			if(result==0)
				return true;
			if(result<0) {
				Node left = getChildNode(i);
				if (left != null)
					return left.containsKey(key);
				else return false;
			}

		}
		Node right=getChildNode(i);
		if(right==null)
	    	return false;
		else return right.containsKey(key);
	}

	public void checkParents(){
	    int children=size()+1;
	    for(int i=0;i<children;++i){
			Node child = getChildNode(i);
			if(child==null)
				return;
	    	if(child.getParent()!=this)
	    		throw new RuntimeException("a child node has wrong parent");
	    	child.checkParents();
		}
	}
}
