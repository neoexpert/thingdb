package neo.db.btree;

public class NodeImpl<T extends Comparable> extends Node<T>{
	Node parent;
	T[] keys;
	Node[] nodes;	
	int size=0;
	public NodeImpl(ABTree tree){
		super(tree);
		keys=(T[])new Comparable[3];
		nodes=new NodeImpl[4];
	}

	public NodeImpl(ABTree tree, int size, T[] keys, Node[] pointers){
		super(tree);
		this.size=size;
		this.keys=keys;
		nodes=pointers;
		int psize=size+1;
		for(int i=0;i<psize;++i){
			Node child = pointers[i];
			if(child==null)return;
			child.setParent(this);
		}
	}

	public T getKey(int index){
		return keys[index];
	}

	public void setKey(int index, T key){
		keys[index]=key;
	}

// #1#2#
// 012
// n###
	public void insert(int index, T key){
		System.arraycopy(keys, index, keys, index+1, size-index);
		//System.arraycopy(nodes,index, nodes, index+1, size+1-index);
		/*
		for(int i=index;i<size;++i){
				keys[i+1]=keys[i];
				nodes[i+1]=nodes[i];
			}*/
			keys[index]=key;
			++size;
	}

	public int insert(T key){
		int i=0;
		for(;i<size;++i){
			if(key.compareTo(keys[i])<=0)
				break;
		}
		insert(i, key);
		return i;
	}
	
	public  Node getChildNode(int index){
		return nodes[index];
	}

	@Override
	public Node createChildNode(int index, int size, T[] keys, Node[] pointers){
		NodeImpl ni;
		if(keys==null)
			ni=new NodeImpl(tree);
		else
			ni=new NodeImpl(tree, size, keys, pointers);
		ni.parent=this;
		Node prev=nodes[index];
		if(prev!=null){
			System.arraycopy(nodes, index, nodes, index+1, this.size-index);
		}

		nodes[index]=ni;
		++size;
		return ni;
	}

	public Node getParent(){
		if(parent==null){
			parent=new NodeImpl(tree);
			tree.setRoot(parent);
		}
		return parent;
	}

	@Override
	protected void deletePoiner(int index) {
		System.arraycopy(nodes, index+1, nodes, index, size-index);
	}

	@Override
	protected void removeChild(Node<T> child) {
		int children=size+1;
		for(int i=0;i<children;++i){
			if(nodes[i]==child){

			}
		}
	}

	@Override
	protected void setParent(Node<T> node) {
		this.parent=node;
	}

	public int size(){
		return size;
	}

	public String toString(){
		int maxSize=tree.maxSize;
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<keys.length;++i){
			sb.append(keys[i])
				.append(",");
		}
		sb.append("\n");
		for(int i=0;i<nodes.length;++i){
			if(nodes[i]==null)
				sb.append('0');
			else
				sb.append("x");
		}
		return sb.toString();
	}
}
