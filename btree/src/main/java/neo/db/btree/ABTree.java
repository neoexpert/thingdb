package neo.db.btree;

public abstract class ABTree<T extends Comparable>{
	protected final int maxSize=3;
	protected final int halfSize=maxSize/2;
	protected Node root;
	public void setRoot(Node node){
		this.root=node;
	}
	public String toString(){
		return root+"";
	}
	public void add(T key){
		root.add(key);
	}

	public boolean containsKey(T key){
		return root.containsKey(key);
	}

	public void checkParents(){
		root.checkParents();
	}
}
