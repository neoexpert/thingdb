/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package neo.db.btree;

import java.io.*;
import java.util.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.junit.*;
import org.junit.runners.MethodSorters;

/**
 * Unit test for simple App.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BTreeTest
		extends TestCase {

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public BTreeTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(BTreeTest.class);
	}

	private final int amount = 50;


	public void test0BTreeBasics() throws IOException, Exception {
		Random r=new Random();
		int min=Integer.MAX_VALUE;
		for(int i=0;i<16;++i) {
		    long seed=r.nextLong();
		    r=new Random(seed);
			BTreeImpl<Integer> bt = new BTreeImpl<>();
			for (int j = 0; j < 1014*8; ++j) {
				try {
					int key=r.nextInt(16);
					bt.add(key);
					bt.checkParents();
					if(!bt.containsKey(key)){
						throw new RuntimeException("does not contain key: "+key);
					}
				}
				catch (Throwable e){
					if(min>j) {
						min = j;
						System.out.println("min: "+min+" for seed: "+seed);
					}
				}
			}
		}

	}

	public void test1BTreeBasics() throws IOException, Exception {
		Random r = new Random(-6066304521273584200L);
		BTreeImpl<Integer> bt = new BTreeImpl<>();
		for (int j = 0; j < 100; ++j) {
			try {
				int key=r.nextInt(16);
				System.out.println(key);
				bt.add(key);
				bt.checkParents();
				assertTrue(bt.containsKey(key));
			}
			catch (Throwable e){
			    e.printStackTrace();
			}
		}

	}
	//12 8 5 8 8 0
}
