/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package neo.db;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.junit.*;
import org.junit.runners.MethodSorters;

/**
 * Unit test
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StorageTest
		extends TestCase {

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public StorageTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(StorageTest.class);
	}

	private final int amount = 50;


	public void test0Simple() throws IOException{
		File f=new File("db");
		if(f.exists()){
			new File(f,"db").delete();
			new File(f,"hm").delete();
		}
		Storage s=new Storage(f);
		long id1=s.insert("huhu".getBytes());
		assertEquals(4L, s.getPayLoadSize());
		s.check();
		long id2=s.insert("huhu".getBytes());
		assertEquals(8L, s.getPayLoadSize());
		s.check();
		s.delete(id1);
		assertEquals(4L, s.getPayLoadSize());
		s.check();
		s.compact();
		s.check();
	}

	public void test0Storage() throws IOException{
		File f=new File("db");
		if(f.exists()){
			new File(f,"db").delete();
			new File(f,"hm").delete();
		}
		Random r=new Random();
		for(int k=0;k<8;++k){
			System.out.println("test :"+k);
			ArrayList<Long> toDelete=new ArrayList<>();
			try(Storage s=new Storage(f)){

				for(int i=0;i<1024;++i){
					byte[] value=new byte[r.nextInt(1024*8)+1];
					r.nextBytes(value);
					long id = s.insert(value);
					byte[] got = s.get(id);
					assertTrue(Arrays.equals(value, got));
					if(r.nextBoolean())
						s.delete(id);
					else toDelete.add(id);
				}
			}
			try(Storage s=new Storage(f)){
				s.compact();
				s.check();
				for(long id:toDelete){
					s.delete(id);
				}
			}
			try(Storage s=new Storage(f)){
				s.compact();
				s.check();
				assertEquals(0L, s.getPayLoadSize());
			}
		}
	}

	public void test0StorageConsistencyDeletion() throws IOException{
		File f=new File("db");
		if(f.exists()){
			new File(f,"db").delete();
			new File(f,"hm").delete();
		}
		Storage s=new Storage(f);
		long id=s.insert("test".getBytes());
		s.simulateDeletionFailure(id);
		s=new Storage(f);
		assertNull(s.get(id));
		s.check();
		s.compact();
		s.check();
	}

	public void test0StorageConsistencyInsertion() throws IOException{
		File f=new File("db");
		if(f.exists()){
			new File(f,"db").delete();
			new File(f,"hm").delete();
		}
		Storage s=new Storage(f);
		long id=s.insert("test".getBytes());
		s.simulateInsertFailure();
		s=new Storage(f);
		s.check();
		s.compact();
		s.check();
	}

	public void test0StorageConsistencyMoving() throws IOException{
		File f=new File("db");
		if(f.exists()){
			new File(f,"db").delete();
			new File(f,"hm").delete();
		}
		Storage s=new Storage(f);
		s.simulateMovingFailure();
		s=new Storage(f);
		s.check();
		s.compact();
		s.check();
	}

}
