package neo.db;
/*
    created by neoexpert at 08.02.21
*/

import java.io.*;
import java.util.function.LongConsumer;

public class Storage implements AutoCloseable {
  private static final long DELETED = -1;
  /**
   * meta header:
   * struct{
   * int64 id;
   * int64 payloadSize;
   * int64 garbageSize;
   * int8  operation;
   * int64 operand1;
   * int64 operand1_copy;
   * int64 operand2;
   * int64 operand2_copy;
   * }
   */
  private static final int FIRST = 8 + 8 + 8 + 1 + 8 + 8 + 8 + 8;
  private static final int META_LENGTH = 8 + 1;
  private static final byte OP_APPEND = 1;
  private static final byte OP_MOVE = 17;
  private static final byte OP_DELETE = 65;
  private static final byte RAW_DATA = 0;
  final RandomAccessFile raf;
  private final File path;
  private final DHashMap index;
  private long idCounter = 0;
  private long payLoadSize;
  private long garbageSize;

  public Storage(String dbPath) throws IOException {
    this(new File(dbPath));
  }

  public Storage(File f) throws IOException {
    this.path = f;
    boolean create = false;
    if (!path.exists()) {
      path.mkdirs();
      create = true;
    }
    File dbFile = new File(path, "db");
      if (!dbFile.exists()) {
          create = true;
      }
    File hmFile = new File(path, "hm");
    raf = new RandomAccessFile(dbFile, "rw");
    if (create) {
      writeMeta();
        if (hmFile.exists()) {
            throw new RuntimeException("hm file exists, but db file is missing");
        }
      index = new DHashMap(hmFile);
    } else {
      boolean ok = readMeta();
        if (!ok) {
            fix();
            if (hmFile.exists()) {
                if (!hmFile.delete()) {
                    throw new RuntimeException("could not delete possible broken index file: hm");
                }
            }
            index = new DHashMap(hmFile);
            rebuildIndex();
        } else {
            index = new DHashMap(hmFile);
        }
    }
    markOpen();
  }


  private void rebuildIndex() throws IOException {
    iterate(pos -> {
      try {
        raf.seek(pos);
        int length = raf.readInt();
        long id = raf.readLong();
          if (id < -1) {
              throw new RuntimeException(String.format("check failed: invalid id read: %d", id));
          }
        index.put(id, pos);
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
  }

  private void writeMeta() throws IOException {
    raf.seek(0L);
    raf.writeLong(idCounter);
    raf.writeLong(payLoadSize);
    raf.writeLong(garbageSize);
    raf.writeByte(0);
    raf.writeLong(0);
    raf.writeLong(0);
    raf.writeLong(0);
    raf.writeLong(0);
  }

  private void writeOP(byte op, long p1, long p2) throws IOException {
    raf.seek(8 + 8 + 8);
    raf.writeByte(op);
    raf.writeLong(p1);
    raf.writeLong(p1);
    raf.writeLong(p2);
    raf.writeLong(p2);
  }

  private void commit() throws IOException {
    //clear op:
    raf.seek(8 + 8 + 8);
    raf.writeByte(0);
    raf.writeLong(0);
    raf.writeLong(0);
    raf.writeLong(0);
    raf.writeLong(0);
  }

  public void sync() throws IOException {
    synchronized (raf) {
      raf.getFD().sync();
    }
  }

  private boolean readMeta() throws IOException {
    raf.seek(0L);
    idCounter = raf.readLong();
    if (idCounter == -1) {
      raf.skipBytes(8 + 8);
      byte op = raf.readByte();
      switch (op) {
        case OP_APPEND:
          long p1 = raf.readLong();
          long p1c = raf.readLong();
          if (p1 == p1c && p1 != 0L) {
            System.err.println(
                "storage file was not closed properly, last insert operation was failed, fixing...");
            long len = raf.length();
              if (p1 > len) {
                  throw new RuntimeException(
                      "could not fix the database file: invalid length (p1>len)");
              }
            raf.setLength(p1);
            System.err.println(
                "dropping probably invalid insertion data: " + (len - p1) + " bytes");
          }
          commit();
          break;
        case OP_MOVE:
          p1 = raf.readLong();
          p1c = raf.readLong();
          long p2 = raf.readLong();
          long p2c = raf.readLong();
            if (p1 == p1c && p2 == p2c && p1 != 0L && p2 != 0L) {
                move(p1, p2);
            }
          break;
        case OP_DELETE:
          p1 = raf.readLong();
          p1c = raf.readLong();
            if (p1 == p1c && p1 != 0L) {
                deleteAt(p1);
            }
          break;
      }
      return false;
    }
    payLoadSize = raf.readLong();
    garbageSize = raf.readLong();
    return true;
  }

  private void markOpen() throws IOException {
    raf.seek(0L);
    raf.writeLong(-1);
  }

  public long insert(byte[] arr) throws IOException {
    return insert(RAW_DATA, arr);
  }

  private long insert(byte type, byte[] arr) throws IOException {
    long id = ++idCounter;
    long pos;
    synchronized (raf) {
      pos = raf.length();
      writeOP(OP_APPEND, pos, 0);
      raf.seek(pos);
      raf.writeInt(arr.length + META_LENGTH);
      raf.writeLong(id);
      raf.writeByte(type);
      raf.write(arr);
      payLoadSize += arr.length;
      index.put(id, pos);
      commit();
    }
    return id;
  }

  public byte[] get(long id) throws IOException {
    synchronized (raf) {
      Long pos = index.get(id);
        if (pos == null) {
            return null;
        }
      raf.seek(pos);
      int length = raf.readInt();
      //skip id
      raf.skipBytes(8);
      byte type = raf.readByte();
        if (type != RAW_DATA) {
            throw new RuntimeException("not a raw data object");
        }
      byte[] result = new byte[length - META_LENGTH];
      raf.readFully(result);
      return result;
    }
  }

  void simulateMovingFailure() throws IOException {
    long id1 = insert("test".getBytes());
    long id2 = insert("test".getBytes());
    long to = index.get(id1);
    long from = index.get(id2);
    synchronized (raf) {
      delete(id1);
      writeOP(OP_MOVE, from, to);
      closed = true;
      raf.close();
    }
  }


  private static class IterationResult {
    final long payloadSize;
    final long garbageSize;
    final long maxID;

    private IterationResult(long payloadSize, long garbageSize, long maxID) {
      this.payloadSize = payloadSize;
      this.garbageSize = garbageSize;
      this.maxID = maxID;
    }
  }

  private IterationResult iterate(LongConsumer consumer) throws IOException {
    long maxID = -1;
    synchronized (raf) {
      long payload = 0L;
      long holesSize = 0L;
      long fLength = raf.length();
      long pos = FIRST;
      while (pos < fLength) {
        raf.seek(pos);
        int length = raf.readInt();
          if (length < 0) {
              throw new RuntimeException(
                  String.format("iteration failed: the length value at %d is negative. length: %d",
                      pos,
                      length));
          }
        long id = raf.readLong();
          if (id != DELETED) {
              if (maxID < id) {
                  maxID = id;
              }
              payload += (length - META_LENGTH);
              consumer.accept(pos);
          } else {
              holesSize += (length - META_LENGTH);
          }
        pos += length + 4;
      }
        if (pos != fLength) {
            throw new RuntimeException("error at end of iteration: pos!=fLength");
        }
      return new IterationResult(payload, holesSize, maxID);
    }
  }

  private void fix() throws IOException {
    IterationResult result = iterate(pos -> {
      try {
        raf.seek(pos);
        int length = raf.readInt();
        long id = raf.readLong();
          if (id < -1) {
              throw new RuntimeException(
                  String.format("check failed: invalid id was read: %d", id));
          }

      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    });
    payLoadSize = result.payloadSize;
    garbageSize = result.garbageSize;
    idCounter = result.maxID + 1;
  }

  public void check() throws IOException {
    synchronized (raf) {
        if (compacting) {
            return;
        }
      IterationResult result = iterate(pos -> {
        try {
          raf.seek(pos);
          int length = raf.readInt();
          long id = raf.readLong();
            if (id < -1) {
                throw new RuntimeException(
                    String.format("check failed: invalid id was read: %d", id));
            }

        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      });
        if (result.payloadSize != payLoadSize) {
            throw new RuntimeException("check failed: wrong payload size");
        }
        if (result.garbageSize != garbageSize) {
            throw new RuntimeException("check failed: wrong garbage size");
        }
    }
  }

  private class PartialInputStream extends InputStream {
    private long pos;
    private final long to;

    public PartialInputStream(long from, long to) {
      this.pos = from;
      this.to = to;
    }

    @Override
    public int read() throws IOException {
        if (pos >= to) {
            return -1;
        }
      synchronized (raf) {
        raf.seek(pos++);
        return raf.read();
      }
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        if (pos >= to) {
            return -1;
        }
        if (pos + len > to) {
            len = (int) (to - pos);
        }
      raf.seek(pos);
      pos += len;
      int readBytes = raf.read(b, off, len);
        if (readBytes != len) {
            throw new IOException("internal raf error: readBytes!=len");
        }
      return len;
    }
  }

  public InputStream getInputStream(long id) throws IOException {
    synchronized (raf) {
      long pos = index.get(id);
      raf.seek(pos);
      int length = raf.readInt() - 8;
      return new PartialInputStream(pos + 12, pos + 12 + length);
    }
  }

  private byte[] getRaw(long pos) throws IOException {
    raf.seek(pos);
    int length = raf.readInt();
    byte[] result = new byte[length];
    raf.read(result);
    return result;
  }

  private void putRaw(long to, byte[] data) throws IOException {
    raf.seek(to);
    raf.writeInt(data.length);
    raf.write(data);
  }

  public void delete(long id) throws IOException {
    synchronized (raf) {
      long pos = index.delete(id);
      deleteAt(pos);
    }
      if (garbageSize * 2 > payLoadSize) {
          compactAsync();
      }
    //System.out.println("too much garbage");
  }

  private void deleteAt(long pos) throws IOException {
    writeOP(OP_DELETE, pos, 0);
    raf.seek(pos);
    int pLength = raf.readInt() - META_LENGTH;
    payLoadSize -= (pLength);
    garbageSize += pLength;
    raf.writeLong(DELETED);
    commit();
  }

  void simulateInsertFailure() throws IOException {
    synchronized (raf) {
      long pos = raf.length();
      writeOP(OP_APPEND, pos, 0);
      raf.seek(pos);
      raf.writeInt(1234);
      raf.writeInt(123);
      raf.write(new byte[] {1, 2, 3, 4, 5, 6, 7, 8, 9});
      index.put(127, pos);
      closed = true;
      raf.close();
    }
  }

  void simulateDeletionFailure(long id) throws IOException {
    synchronized (raf) {
      long pos = index.delete(id);
      writeOP(OP_DELETE, pos, 0);
      closed = true;
      raf.close();
    }
  }

  private volatile boolean closed = false;

  public void close() throws IOException {
    synchronized (raf) {
      writeMeta();
      raf.close();
      index.close();
      closed = true;
    }
  }

  public void compactAsync() {
      if (compacting) {
          return;
      }
    new Thread(() -> {
      try {
        compact();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }).start();
  }

  private volatile boolean compacting = false;

  public void compact() throws IOException {
    synchronized (raf) {
        if (compacting) {
            return;
        }
      compacting = true;
    }
    long fLength;
    synchronized (raf) {
        if (closed) {
            return;
        }
      fLength = raf.length();
    }
    long pos = FIRST;
    long holeSize = 0;
    long nextDeleted = findDeleted(pos, fLength);
    if (nextDeleted == -1) {
      synchronized (raf) {
        compacting = false;
      }
      return;
    }

    while (true) {
      synchronized (raf) {
          if (closed) {
              return;
          }
        long next = findNext(nextDeleted, raf.length());
        if (next == -1) {
          garbageSize = 0;
          raf.setLength(nextDeleted);
          compacting = false;
          return;
        }
        holeSize = next - nextDeleted - 4;
        int movedLength = moveAndIndex(next, nextDeleted);
        nextDeleted = nextDeleted + movedLength + 4;
        raf.seek(nextDeleted);
        raf.writeInt((int) holeSize);
        raf.writeLong(DELETED);
      }
    }
  }

  private int move(long from, long to) throws IOException {
    writeOP(OP_MOVE, from, to);
    byte[] data = getRaw(from);
    int type = data[8];
    putRaw(to, data);
      if (type != RAW_DATA) {
          onMoved(type, data, to);
      }
    commit();
    return data.length;
  }

  private void onMoved(int type, byte[] obj, long to) {
  }

  private int moveAndIndex(long from, long to) throws IOException {
    int len = move(from, to);
    raf.seek(to);
    //skip length
    raf.skipBytes(4);
    long id = raf.readLong();
    index.put(id, to);
    return len;
  }

  protected long findDeleted(long pos, long max) throws IOException {
    while (pos < max) {
      synchronized (raf) {
          if (closed) {
              return -1;
          }
        raf.seek(pos);
        int length = raf.readInt();
        long id = raf.readLong();
        if (id == DELETED) {
          return pos;
        }
        raf.skipBytes(length);
        pos += (length + 4);
      }
    }
    return -1;
  }

  protected long findNext(long pos, long max) throws IOException {
    while (pos < max) {
      synchronized (raf) {
          if (closed) {
              return -1;
          }
        raf.seek(pos);
        int length = raf.readInt();
        long id = raf.readLong();
        if (id != DELETED) {
          return pos;
        }
        raf.skipBytes(length);
        pos += (length + 4);
      }
    }
    return -1;
  }

  public long getPayLoadSize() {
    return payLoadSize;
  }
}
