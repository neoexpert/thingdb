/*
 * Copyright (C) 2018 neoexpert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package neo.db;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Hello world! persistent <Integer,Integer> HashMap :-) 0-key is not allowed
 */
public class DHashMap implements AutoCloseable {

  File f;
  RandomAccessFile raf;

  void clear() {
  }

  private long size = 2;

  /**
   * @param f
   * @throws IOException
   */
  public DHashMap(File f) throws IOException {
    this(f, 2);
  }

  private DHashMap(File f, long size) throws IOException {
    this.size = size;
    this.f = f;
    loadRAF();
  }

  /**
   * @throws IOException
   */
  public final void loadRAF() throws IOException {
    raf = new RandomAccessFile(f, "rw");
    if (raf.length() == 0) {
      raf.setLength(size * 16);
    } else {
      if (raf.length() % 16 != 0) {
        throw new RuntimeException("corrupt DHashMap file");
      }
      size = (int) (raf.length() / 16);
    }
  }

  /**
   * @param key
   * @return
   * @throws IOException
   */
  public Long delete(long key) throws IOException {
    if (key == 0) {
      throw new RuntimeException("key cant be 0");
    }
    long v = 0;
    long pos = key;
    int shift = 1;
    int i = 0;
    for (; i < 16; i++) {
      pos = (pos % size + size) % size;
      raf.seek(pos * 16);
      long rkey = raf.readLong();
      if (rkey == key) {
        v = raf.readLong();
        raf.seek(pos * 16);
        raf.writeLong(0L);
        raf.writeLong(0L);
        break;
      }
      pos += shift;
      shift += 2;
    }
    for (; i < 16; i++) {
      //pos = pos%size;
      pos = (pos % size + size) % size;
      raf.seek(pos * 16);
      long rkey = raf.readLong();
      if (rkey == 0) {
        //raf.writeLong(0);
        //raf.writeLong(0);
        continue;
      } else if (rkey == key) {
        long value = raf.readLong();
        raf.seek((pos + 1) * 16);
        raf.writeLong(rkey);
        raf.writeLong(value);
        raf.seek(pos * 16);
        raf.writeLong(0L);
        raf.writeLong(0L);
      }
      pos += shift;
      shift += 2;
    }
    return v;
  }

  /**
   * @param key
   * @param value
   * @throws IOException
   */
  public final void put(long key, long value) throws IOException {
    if (key == 0) {
      throw new RuntimeException("key cant be 0");
    }
    long pos = key;
    int shift = 1;
    for (int i = 0; i < 16; i++) {
      //pos = pos%size;
      pos = (pos % size + size) % size;
      raf.seek(pos * 16);
      long rkey = raf.readLong();
      if (rkey == 0L || rkey == key) {
        raf.seek(pos * 16);
        raf.writeLong(key);
        raf.writeLong(value);
        return;
      }
      pos += shift;
      shift += 2;
    }
    increase();
    put(key, value);
  }

  /**
   * @param e
   * @throws IOException
   */
  public void put(Entry e) throws IOException {
    put(e.key, e.value);
  }

  /**
   * @param key
   * @return
   * @throws IOException
   */
  public Long get(long key) throws IOException {
    if (key == 0L) {
      throw new RuntimeException("key cant be 0");
    }
    long pos = key;
    int shift = 1;
    for (int i = 0; i < 16; i++) {
      //pos = pos%size;
      pos = (pos % size + size) % size;
      raf.seek(pos * 16);
      long rkey = raf.readLong();
      if (rkey == key) {
        return raf.readLong();
      }
      if (rkey == 0) {
        return null;
      }
      pos += shift;
      shift += 2;
    }
    return null;
  }

  /**
   * @throws Exception
   */
  @Override
  public void close() throws IOException {
    raf.close();
  }

  public static class Entry {

    long key;
    long value;

    /**
     * @param key
     * @param value
     */
    public Entry(long key, long value) {
      this.key = key;
      this.value = value;
    }
  }

  /**
   * @param pos
   * @return
   * @throws IOException
   */
  public Entry getEntry(long pos) throws IOException {
    raf.seek(pos * 16);
    return new Entry(raf.readLong(), raf.readLong());
  }

  private void increase() throws IOException {
    File tmp_file = File.createTempFile("tmp", "tmp");
    try (DHashMap tmp = new DHashMap(tmp_file, size * 2)) {
      for (int i = 0; i < size; i++) {
        Entry e = getEntry(i);
        if (e.key != 0) {
          tmp.put(e);
        }
      }


    } catch (IOException ex) {
      Logger.getLogger(DHashMap.class.getName()).log(Level.SEVERE, null, ex);
      throw ex;
    }
    raf.close();
    Files.move(tmp_file.toPath(), f.toPath(), StandardCopyOption.REPLACE_EXISTING);

    loadRAF();
  }
}
